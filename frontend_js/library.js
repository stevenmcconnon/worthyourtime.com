// JavaScript Document

function setLogout () {
	// click handler for log out
	var logoutUrl = "/backend/login.php?do=logout";
	$('#linkLogout').click(function() {
		$.getJSON(logoutUrl,
			function(responseText){
				console.log("response is " + responseText);
				if (responseText[0] == '0') {
					alert(responseText[1]);
				} else {
					if (responseText[0] == '1') {
						window.location = "/";
					}
				}
			}
		)
	});
}

$(document).ready(function() {

//	$('#username').html(json.username);

	// click handler for log out id on logout link must be linkLogout
	if (json.group) {
		setLogout();
//		$('#linkLogout').html("Logout(" + json.username + ")");
		$('#linkLogout').html("Logout");
		$("#linkLogout").attr("href", "#")
		$('#linkSignUp').html("Dashboard");
		$("#linkSignUp").attr("href", "dashboard/dashboard.php")
	}
});