<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('./includes/initilization.php');	
$isLoggedIn = $currentUser->IsLoggedIn();

if($isLoggedIn != 1)
	$isLoggedIn = 0;
	
$currentUser->resetToken();
$info = $currentUser->retJSONInfo();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sugar Daddy Online Dating</title>
  <meta charset="utf-8">
  <meta name="description" content="Worth Your Time is a Sugar Daddy dating site. Find, date, search, meet or get a sugar daddy or sugar baby. " />
  
  <meta name="keywords" content="online dating, sugar daddy, sugar baby, free dating, paid to date, paid dating, make money dating, dating for money, discreet dating, date hot girls, college girls, college singles, cougar dating, seeking arrangement, sugar daddy relationship, extra marital dating" />

  <link rel="canonical" href="http://www.worthyourtime.com" />
  <link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/grid.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"> 
  <script type="text/javascript" src="js/jquery-1.4.2.min.js" ></script>
<script type="text/javascript" src="js/contact-form.js"></script>
<!--[if lt IE 7]><div style=' clear: both; height: 59px; text-align:center; position: relative;'> <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" height="42" width="820" alt="" /></a></div><![endif]-->
<!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]-->
<!--[if lt IE 9]><link rel="stylesheet" href="css/ie_style.css" type="text/css" media="screen"><![endif]-->
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>/js/jquery.jqtransform.js" type="text/javascript"></script>
<script type="text/javascript" src="js/hover-image.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript">


$(document).ready(function() {
   $('ul.sf-menu').superfish();
   
     //makes the form look good
   $("#form1").jqTransform();
   
   //makes the element hidden at first
   $("#minimum").hide();
   
	var json = <? echo $info; ?>;
	if (json.group != 0 ) 
		console.log("logged in! " + json.username + "," + json.id + ", " + json.token + ", " + json.group); 

	var data; 
	
	//this is to show the hidden div for generous members
	$("div.jqTransformSelectWrapper ul li a").click(function(){
    	var value= $("div.jqTransformSelectWrapper span").text()
    	
    	    	
    	//hide/show based on member type selected
    	if ( value.match(/Generous Member/gi) ) {
			$("#minimum").hide("slow");
    	} else if ( value.match(/Attractive Member/gi) ) {  
    		$("#minimum").show("slow");
    	}  	
    	return false; //prevent default browser action
	});
	

	$('#btnJoinNow').click(function(event) {
		event.preventDefault();
		processRegistration ();
	});
	
	
	
	
	function processRegistration () {
	    var $form = $( '#form1' );
	    var errors = false;
        username1 = $form.find( 'input[name="username"]' ).val();
		password1 = $form.find( 'input[name="password"]' ).val();
		passwordVerify1 = $form.find( 'input[name="passwordVerify"]' ).val();
		zip1 = $form.find( 'input[name="zip"]' ).val();
		email1 = $form.find( 'input[name="email"]' ).val();
		age1 = $form.find( 'input[name="age"]' ).val();
		iam1 = $('#iam').val();
		min1 = $('#min').val();
		seek1 = $('#seek').val();
		groupNo = $('#groupNo').val();
		terms = $('#terms').attr('checked'); 
		submit1 = "submit";
	    
	    
	    
	    //clears the errors and preps it for new errors
	    $("#errorMessages").html("<b>Errors:</b><br/>");


		recaptcha_challenge_field1 = $form.find( 'input[name="recaptcha_challenge_field"]' ).val()
		recaptcha_response_field1 = $form.find( 'input[name="recaptcha_response_field"]' ).val()

		if (!username1) {
			$("#errorMessages").append("* Please enter a username<br/>");
			errors = true;
		} if (!password1) {
			$("#errorMessages").append("* Please enter a password<br/>");
			errors = true;
		} if (!passwordVerify1) {
			$("#errorMessages").append("* Please enter a password confirmation<br/>");
			errors = true;
		} if (!zip1) {
			$("#errorMessages").append("* Please enter a zip code<br/>");
			errors = true;
		} if (!email1) {
			$("#errorMessages").append("* Please enter an email address<br/>");
			errors = true;
		} if (!age1) {
			$("#errorMessages").append("* Please enter an age<br/>");
			errors = true;
		} if (!iam1) {
			$("#errorMessages").append("* Please enter a gender<br/>");
			errors = true;
		} if (!seek1) {
			$("#errorMessages").append("* Please enter gender seeking<br/>");
			errors = true;
		} if (!groupNo) {
			$("#errorMessages").append("* Please enter a your user type<br/>");
			errors = true;
		} if (!terms) {
			$("#errorMessages").append("* You must agree to the terms of service<br/>");
			errors = true;
		} if (!min1 && groupNo != 2) {
			$("#errorMessages").append("* You must enter the minimum offer you will accept<br/>");
			errors = true;
		}
		
		
		if (errors == false) {
			$.post(
				"backend/register.php",
				{ username: username1, password: password1, submit: submit1,
				passwordVerify: passwordVerify1, zip: zip1, email: email1,
				age: age1, iam: iam1, seek: seek1, group: groupNo, 
				recaptcha_challenge_field: recaptcha_challenge_field1,
				recaptcha_response_field : recaptcha_response_field1, minOffer: min1  },
				function(responseText){
					if (responseText[0] == '0') {
						console.log("Failed to register: " + responseText[1]);
						if ( JSON.stringify(responseText[1]).match(/username/gi) ) {
							alert("* Your username or email address already exists, please change and try again.");
						} if ( JSON.stringify(responseText[1]).match(/captcha/gi) ) {
							alert("* The Captcha was invalid, please try again.");
						}
					} else { 
						if (responseText[0] == '1') {
							console.log("successfully registered");
						}
						$.post(
							"backend/login.php",
							{ username: username1, password: password1  },
							function(responseText){
								if (responseText[0] == '0') {
									console.log("Error logging in");
								} else {
									if (responseText[0] == '1') {
										window.location = "dashboard/dashboard.php"
									}
								}
							},
							"json"
						);
					}
				},
				"json"
			);
		}
		
		if(errors == true) {
			$("#errorMessages").show();
			
			//scrolls to the errorMessages div
			$('body').scrollTop( $("#errorMessages").offset().top - 50 );
			
		}
	}

   
 
});
</script>  
<style type="text/css">
.style1 {color: #FF0000}
</style>
</head>
<body>
<?php include_once("header.php"); ?>

<aside class="aside2"></aside>
<p><img src="images/SilverBanner.png" width="1242" height="62"></p>
<section id="content">
    <p>&nbsp;</p>
    <div class="main">
      <div class="inside">
          <div class="container_24">
           	  <div class="suffix_1">
           	    <div class="grid_12 alpha">
           	      <div class="box">
           	        <div class="indent-box">
           	          <h2>We're 100% Free to join!</h2>
                      <div id="result"></div>
					 <div id="errorMessages">
								<!-- This is where our error messages will go. -->
							<b>Errors:</b>
							<br/>
						</div>
           	          <p>&nbsp;</p>
           	          <form method="POST" id="form1">
           	            <fieldset>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Choose a username: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="username" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Choose a password: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input type="password" name="password" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> repeat password: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input type="password" name="passwordVerify" type="password" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> E-mail address: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="email" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p><br class="clear" />
       	                    </p>
                          </div>
                          
                          <div class="container1 row2">
           	                <div class="col-1 txt2">   </div>
           	                <div class="col-2">
           	                  <div class="rowElem2" >
           	                    <select id="groupNo" name="groupNo" class="select" >
           	                      <option value="1">Generous Member</option>
           	                      <option value="2">Attractive Member</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p><br class="clear" />
       	                    </p>
                          </div>
                          
           	              <div class="container1 row2" id='minimum' style="display:;">
           	                <div class="col-1 txt2"> Minimum Amount  </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                  		<select id="min" name="min">
           	                  			<option value="50">$50</option>
           	                  			<option value="100">$100</option>
           	                  			<option value="200">$200</option>
           	                  			<option value="300">$300</option>
           	                  			<option value="400">$400</option>
           	                  			<option value="500">$500</option>
           	                  			<option value="600">$600+</option>
           	                  		</select>     	                      
           	                  	</div>
       	                    </div>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p><br class="clear" />
       	                    </p>
                          </div>
                        
           	              <div class="container1 row2">
           	                <div class="col-3 txt2"> and I'm a </div>
           	                <div class="col-4">
           	                  <div class="rowElem">
           	                    <select name="iam" class="select2" id="iam" >
           	                      <option value="1">Man</option>
           	                      <option value="2">Woman</option>
                                </select>
       	                      </div>
       	                    </div>
           	                <div class="col-5 txt2"> Seeking a</div>
           	                <div class="col-7">
           	                  <div class="rowElem">
           	                    <p>
           	                      <select name="seek" class="select2" id="seek" >
           	                        <option value="2">Women</option>
           	                        <option value="1">Man</option>
                                  </select>
       	                        </p>
           	                    <p>&nbsp;</p>
       	                      </div>
       	                    </div>
           	                <p><span class="rowElem"> &nbsp; </span></p>
           	                <p>&nbsp;</p>
       	                  </div>
           	              <p>&nbsp;</p>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> My age is </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="age" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p><br class="clear" />
       	                    </p>
                          </div>
<!--           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> My birthday: </div>
           	                <div class="col-6">
           	                  <div class="rowElem">
           	                    <select name="select2" class="select4" >
           	                      <option value="">01</option>
           	                      <option value="opt1">02</option>
           	                      <option value="opt1">03</option>
           	                      <option value="opt1">04</option>
           	                      <option value="opt1">05</option>
           	                      <option value="opt1">06</option>
           	                      <option value="opt1">07</option>
           	                      <option value="opt1">08</option>
           	                      <option value="opt1">09</option>
           	                      <option value="opt1">10</option>
           	                      <option value="opt1">11</option>
           	                      <option value="opt1">12</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <div class="col-6">
           	                  <div class="rowElem">
           	                    <select name="select2" class="select4" >
           	                      <option value="">01</option>
           	                      <option value="opt1">02</option>
           	                      <option value="opt1">03</option>
           	                      <option value="opt1">04</option>
           	                      <option value="opt1">05</option>
           	                      <option value="opt1">06</option>
           	                      <option value="opt1">07</option>
           	                      <option value="opt1">08</option>
           	                      <option value="opt1">09</option>
           	                      <option value="opt1">10</option>
           	                      <option value="opt1">11</option>
           	                      <option value="opt1">12</option>
           	                      <option value="opt1">13</option>
           	                      <option value="opt1">14</option>
           	                      <option value="opt1">15</option>
           	                      <option value="opt1">16</option>
           	                      <option value="opt1">17</option>
           	                      <option value="opt1">18</option>
           	                      <option value="opt1">19</option>
           	                      <option value="opt1">20</option>
           	                      <option value="opt1">21</option>
           	                      <option value="opt1">22</option>
           	                      <option value="opt1">23</option>
           	                      <option value="opt1">24</option>
           	                      <option value="opt1">25</option>
           	                      <option value="opt1">26</option>
           	                      <option value="opt1">27</option>
           	                      <option value="opt1">28</option>
           	                      <option value="opt1">29</option>
           	                      <option value="opt1">30</option>
           	                      <option value="opt1">31</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <div class="col-7">
           	                  <div class="rowElem">
           	                    <p>
           	                      <select name="select2" class="select5" >
           	                        <option value="">1960</option>
           	                        <option value="opt1">1961</option>
           	                        <option value="opt1">1962</option>
           	                        <option value="opt1">1963</option>
           	                        <option value="opt1">1964</option>
           	                        <option value="opt1">1965</option>
           	                        <option value="opt1">1966</option>
           	                        <option value="opt1">1967</option>
           	                        <option value="opt1">1968</option>
           	                        <option value="opt1">1969</option>
           	                        <option value="opt1">1970</option>
           	                        <option value="opt1">1971</option>
           	                        <option value="opt1">1972</option>
           	                        <option value="opt1">1973</option>
           	                        <option value="opt1">1974</option>
           	                        <option value="opt1">1975</option>
           	                        <option value="opt1">1976</option>
           	                        <option value="opt1">1977</option>
           	                        <option value="opt1">1978</option>
           	                        <option value="opt1">1979</option>
           	                        <option value="opt1">1980</option>
           	                        <option value="opt1">1981</option>
           	                        <option value="opt1">1982</option>
           	                        <option value="opt1">1983</option>
           	                        <option value="opt1">1984</option>
           	                        <option value="opt1">1985</option>
           	                        <option value="opt1">1986</option>
           	                        <option value="opt1">1987</option>
           	                        <option value="opt1">1988</option>
           	                        <option value="opt1">1989</option>
           	                        <option value="opt1">1990</option>
           	                        <option value="opt1">1991</option>
           	                        <option value="opt1">1992</option>
           	                        <option value="opt1">1993</option>
           	                        <option value="opt1">1994</option>
       	                          </select>
       	                        </p>
           	                    <p>&nbsp;</p>
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
-->
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Country </div>
           	                <div class="col-2">
           	                  <div class="rowElem">
           	                    <p>
           	                      <select name="select2" class="select" >
           	                        <option value="">United States</option>
           	                        <option value="opt1">Canada</option>
       	                          </select>
       	                        </p>
           	                    <p>&nbsp;</p>
       	                      </div>
       	                    </div>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p><br class="clear" />
       	                    </p>
                          </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Zip code: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="zip" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
<!--           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Submit New Photo: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
			<input name="file" type="file" /> <br />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
-->           	              
                          
                          
<!--			<form action="/sugar/usercp/backend/adminPhoto.php?do=upload" method="POST" enctype="multipart/form-data"> 
			<input name="file" type="file" /> <br />
			Name: <input name="name" type="text" /> <br />
			Description: <input name="description" type="text" />
			<input name="photoSubmit" type="submit" />
			</form>
-->
                          
                          
                          
                          <div class="container row2">
           	                <div class="col-8">
           	                  <input type="checkbox" class="input1" name="1" id="terms"/>
       	                    </div>
           	                <div class="col-9">
           	                  <p><a href="index-terms.php">I Accept Terms of Use</a></p>
           	                  <p>&nbsp;</p>
           	                </div>
           	                <p>&nbsp;</p>
<p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p>	
                              <?php
          require_once($_SITEROOT_.'includes/classes/recaptchalib.php');
          $publickey = "6LfiL80SAAAAAEe-XdR9nStjbqgc_3mPbIM6Aq1L"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
        ?>
                            </p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <div class="col-2">
                            	<a id="btnJoinNow" href="#" class="but-1">Join now</a>
                            </div>
       	                  </div>
       	                </fieldset>
       	              </form>


       	            </div>
       	          </div>
       	        </div>
           	    <p>&nbsp;</p>
              </div>
            </div>            
        </div>
    </div>
</section>
<footer>      
    <div class="main">
        <div class="inside">
            <div class="container">
                <div class="fleft">
                	<ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="index-search.php">Search</a></li>
                        <li><a href="index-join.php">Join</a></li>
                        <li><a href="index-blog.php">Blog</a></li>
                        <li><a href="index-faq.php">FAQs</a></li>
                        <li><a href="index-contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="fright"><span>Worth Your Time</span> &nbsp;&copy; 2011 &nbsp; &nbsp;<a href="index-privacy.php">Privacy policy</a> &nbsp;<!--{%FOOTER_LINK}--></div>     
            </div>   
        </div>
    </div>   
</footer>    

<script type="text/javascript">
		$(document).ready(function(){			
			//for prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
</script>
<!-- coded by Ann -->
</body>
</html>
