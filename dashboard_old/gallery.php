<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>White Label - a full featured Admin Skin</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/script.js"></script>
	
	
</head>
<body>
				<div id="pageoptions">
			<ul>
				<li><a href="login.html">Logout</a></li>
				<li><a href="#" id="wl_config">Configuration</a></li>
				<li><a href="#">Settings</a></li>
			</ul>
			<div>
						<h3>Place for some configs</h3>
						<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores.</p>
			</div>
		</div>

			<header>
		<div id="logo">
			<a href="test.html">Logo Here</a>
		</div>
		<div id="header">
		  <ul id="headernav">
		    <li>
		      <ul>
		        <li><a href="icons.html">My Account</a><span>300+</span></li>
		        <li><a href="#">Mailbox</a><span>4</span>
		          <ul>
		            <li><a href="#">Offers</a></li>
		            <li><a href="#">Messages</a></li>
		            <li><a href="#">Flirts</a></li>
		            <li><a href="#">Dates</a></li>
	              </ul>
	            </li>
		        <li><a href="wizard.html">Profile Wizard</a><span>Bonus</span></li>
		        <li><a href="#">Buy Credits</a><span>new</span>
		          <ul>
		            <li><a href="error-403.html">403</a></li>
		            <li><a href="error-404.html">404</a></li>
		            <li><a href="error-405.html">405</a></li>
		            <li><a href="error-500.html">500</a></li>
		            <li><a href="error-503.html">503</a></li>
	              </ul>
	            <li><a href="#">Search</a><span>new</span> </li>
	          </ul>
	        </li>
	      </ul>
		  <div id="searchbox">
		    <form id="searchform" autocomplete="off">
		      <input type="search" name="query" id="search" placeholder="Search">
	        </form>
	      </div>
		  <ul id="searchboxresult">
	      </ul>
		  </div>
		</header>
<nav>
			  <ul id="nav">
			    <li class="i_house"><a href="dashboard.html"><span>My Dashboard</span></a></li>
			    <li class="i_book"><a><span>My Offers</span></a>
			      <ul>
			        <li><a href="datatable.html"><span>Offers</span></a></li>
			        <li><a href="doc-breadcrumb.html"><span>Dates</span></a></li>
			        <li><a href="doc-calendar.html"><span>Messages</span></a></li>
			        <li><a href="doc-charts.html"><span>Flirts</span></a></li>
			        <li><a href="doc-dialog.html"><span>Sent</span></a></li>
			        <li><a href="doc-editor.html"><span>Extra</span></a></li>
		          </ul>
		        </li>
			    <li class="i_create_write"><a href="form.html"><span>My Profile</span></a></li>
			    <li class="i_graph"><a href="charts.html"><span>My Account</span></a></li>
			    <li class="i_images"><a href="gallery.html"><span>My Gallery</span></a></li>
			    <li class="i_blocks_images"><a href="widgets.html"><span>Help</span></a></li>
			    <li class="i_breadcrumb"><a href="breadcrumb.html"><span>Who Viewed Me</span></a></li>
			    <li class="i_file_cabinet"><a href="fileexplorer.html"><span>Favorites</span></a></li>
			    <li class="i_calendar_day"><a href="calendar.html"><span>Calendar</span></a></li>
			    <li class="i_speech_bubbles_2"><a href="dialogs_and_buttons.html"><span>Dialogs &amp; Buttons</span></a></li>
		      </ul>
</nav>
	<section id="content">
		
		
		
			<div class="g12">
			<h1>Picture Gallery <span><a href="doc-gallery.html" class="small">Documentation</a></span></h1>
			<p>a sortable gallery with lightbox</p>
			
			<ul class="gallery">
				<li><a href="http://random.revaxarts.com/pool/800x400.jpg" title="Here is your Description"><img src="http://random.revaxarts.com/pool/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/buildings/900x500.jpg" title="some Description"><img src="http://random.revaxarts.com/buildings/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/street/800x400.jpg" title="hit the ESC key to exit the show!"><img src="http://random.revaxarts.com/street/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/turntables/900x400.jpg" title="some Description"><img src="http://random.revaxarts.com/turntables/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/kaffeetasse/800x600.jpg" title="Coffee"><img src="http://random.revaxarts.com/kaffeetasse/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/domino/850x450.jpg" title="some Description"><img src="http://random.revaxarts.com/domino/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/golf/800x400.jpg" title="some Description"><img src="http://random.revaxarts.com/golf/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/subway/300x400.jpg" title="some Description"><img src="http://random.revaxarts.com/subway/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/windows/800x500.jpg" title="some Description"><img src="http://random.revaxarts.com/windows/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/leaf/200x400.jpg" title="some Description"><img src="http://random.revaxarts.com/leaf/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/turntables/800x600.jpg" title="some Description"><img src="http://random.revaxarts.com/turntables/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/streichholz/1200x500.jpg" title="some Description"><img src="http://random.revaxarts.com/streichholz/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/rolltreppe/300x500.jpg" title="you can use the arrow keys to navigate left and right"><img src="http://random.revaxarts.com/rolltreppe/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/street/400x400.jpg" title="hit the ESC key to exit the show!"><img src="http://random.revaxarts.com/street/116x116.jpg" width="116" height ="116" alt=""></a></li>
				<li><a href="http://random.revaxarts.com/kaffeetasse/800x400.jpg" title="Coffee"><img src="http://random.revaxarts.com/kaffeetasse/116x116.jpg" width="116" height ="116" alt=""></a></li>
			</ul>
			
			<pre>
&lt;ul class="gallery"&gt;
	&lt;li&gt;&lt;a href="{pic_url}" title="{optional title}"&gt;&lt;img src="{thumbnail_url}" width="116" height="116" alt=""&gt;&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href="{pic_url}" title="{optional title}"&gt;&lt;img src="{thumbnail_url}" width="116" height="116" alt=""&gt;&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href="{pic_url}" title="{optional title}"&gt;&lt;img src="{thumbnail_url}" width="116" height="116" alt=""&gt;&lt;/a&gt;&lt;/li&gt;
	...
&lt;/ul&gt;</pre>
			
			</div>
			<div class="g12">
			  <h1>Video Gallery <span><a href="doc-gallery.html" class="small">Documentation</a></span></h1>
			  <p>a sortable gallery with lightbox</p>
			  <ul class="gallery">
			    <li><a href="http://random.revaxarts.com/pool/800x400.jpg" title="Here is your Description"><img src="http://random.revaxarts.com/pool/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/buildings/900x500.jpg" title="some Description"><img src="http://random.revaxarts.com/buildings/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/street/800x400.jpg" title="hit the ESC key to exit the show!"><img src="http://random.revaxarts.com/street/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/turntables/900x400.jpg" title="some Description"><img src="http://random.revaxarts.com/turntables/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/kaffeetasse/800x600.jpg" title="Coffee"><img src="http://random.revaxarts.com/kaffeetasse/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/domino/850x450.jpg" title="some Description"><img src="http://random.revaxarts.com/domino/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/golf/800x400.jpg" title="some Description"><img src="http://random.revaxarts.com/golf/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/subway/300x400.jpg" title="some Description"><img src="http://random.revaxarts.com/subway/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/windows/800x500.jpg" title="some Description"><img src="http://random.revaxarts.com/windows/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/leaf/200x400.jpg" title="some Description"><img src="http://random.revaxarts.com/leaf/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/turntables/800x600.jpg" title="some Description"><img src="http://random.revaxarts.com/turntables/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/streichholz/1200x500.jpg" title="some Description"><img src="http://random.revaxarts.com/streichholz/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/rolltreppe/300x500.jpg" title="you can use the arrow keys to navigate left and right"><img src="http://random.revaxarts.com/rolltreppe/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/street/400x400.jpg" title="hit the ESC key to exit the show!"><img src="http://random.revaxarts.com/street/116x116.jpg" width="116" height ="116" alt=""></a></li>
			    <li><a href="http://random.revaxarts.com/kaffeetasse/800x400.jpg" title="Coffee"><img src="http://random.revaxarts.com/kaffeetasse/116x116.jpg" width="116" height ="116" alt=""></a></li>
		      </ul>
			  <pre>
&lt;ul class="gallery"&gt;
	&lt;li&gt;&lt;a href="{pic_url}" title="{optional title}"&gt;&lt;img src="{thumbnail_url}" width="116" height="116" alt=""&gt;&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href="{pic_url}" title="{optional title}"&gt;&lt;img src="{thumbnail_url}" width="116" height="116" alt=""&gt;&lt;/a&gt;&lt;/li&gt;
	&lt;li&gt;&lt;a href="{pic_url}" title="{optional title}"&gt;&lt;img src="{thumbnail_url}" width="116" height="116" alt=""&gt;&lt;/a&gt;&lt;/li&gt;
	...
&lt;/ul&gt;</pre>
	  </div>
			<p>&nbsp;</p>
    </section>
		<footer>Copyright by revaxarts.com 2011</footer>
</body>
</html>