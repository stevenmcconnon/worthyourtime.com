<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('includes/initilization.php');	
$isLoggedIn = $currentUser->IsLoggedIn();

if($isLoggedIn != 1)
	$isLoggedIn = 0;
	
$currentUser->resetToken();
$info = $currentUser->retJSONInfo();



?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/grid.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"> 
  <script type="text/javascript" src="js/jquery-1.4.2.min.js" ></script>
<script type="text/javascript" src="js/contact-form.js"></script>
<!--[if lt IE 7]><div style=' clear: both; height: 59px; text-align:center; position: relative;'> <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" height="42" width="820" alt="" /></a></div><![endif]-->
<!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]-->
<!--[if lt IE 9]><link rel="stylesheet" href="css/ie_style.css" type="text/css" media="screen"><![endif]-->
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script type="text/javascript" src="js/hover-image.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $('ul.sf-menu').superfish();
   
	var jsonUrl = "backend/register.php";
	
	$('#btnJoinNow').click(function() {
//	$('#form1').submit(function() {
	    var $form = $( '#form1' );
        username1 = $form.find( 'input[name="username"]' ).val();
		password1 = $form.find( 'input[name="password"]' ).val();
		passwordVerify1 = $form.find( 'input[name="passwordVerify"]' ).val();
		zip1 = $form.find( 'input[name="zip"]' ).val();
		email1 = $form.find( 'input[name="email"]' ).val();
		age1 = $form.find( 'input[name="age"]' ).val();
		iam1 = $('#iam').val();
		seek1 = $('#seek').val();
		groupNo = $('#groupNo').val();

	    submit1 = "submit";
		//submit = $form.find( 'input[name="submit"]' ).val();
		//alert("submit1 is " + submit1);
		recaptcha_challenge_field1 = $form.find( 'input[name="recaptcha_challenge_field"]' ).val()
		recaptcha_response_field1 = $form.find( 'input[name="recaptcha_response_field"]' ).val()

		var responseItems = [];
		$.post(
			jsonUrl,
			{ username: username1, password: password1, submit: submit1,
			passwordVerify: passwordVerify1, zip: zip1, email: email1,
			age: age1, iam: iam1, seek: seek1, group: groupNo, 
			recaptcha_challenge_field: recaptcha_challenge_field1,
			recaptcha_response_field : recaptcha_response_field1  },
			function(responseText){
				console.log("response is " + responseText);
				if (responseText[0] == '0') {
					alert("Unable to complete registration\n\nProblems with " + responseText[1]);
				}
				if (responseText[0] == '1') {
					alert("Successfull registration!");
					window.location = "dashboard/dashboard.php"
				}
				
				$.each(responseText, function(i,item){
					console.log(" item(" + i + ") is " + item);
				});				
			},
			"json"
		);
		return false;
	});
   
});
</script>  
<style type="text/css">
.style1 {color: #FF0000}
</style>
</head>
<body>
<header>
    <div class="main">
        <h1><a href="index.php">dating</a></h1>
      <div class="indent"><a href="index-login.php" class="but-1">Login</a><a href="index-join.php" class="but-1">sign up</a></div>
        <div class="inside">
         <nav>
                <ul class="sf-menu">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="index-search.php">Search</a>
                    	
                    </li>
                    <li><a href="index-join.php">Join</a></li>
                    <li><a href="index-blog.php">Blog</a></li>
                    <li><a href="index-faq.php">Faqs</a></li>
                    <li></li>
                </ul>
            </nav>
        </div>
    </div>
 
</header>
<aside class="aside2"></aside>
<section id="content">
    <p>&nbsp;</p>
    <div class="main">
    	<a href="#"><img alt="" src="images/banner.jpg" class="banner" /></a>
        <div class="inside">
            <div class="container_24">
           	  <div class="suffix_1">
           	    <div class="grid_12 alpha">
           	      <div class="box">
           	        <div class="indent-box">
           	          <h2>We're 100% Free to join!</h2>
                      <div id="result"></div>

           	          <p>&nbsp;</p>
           	          <form method="POST" id="form1">
           	            <fieldset>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Choose a username: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="username" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Choose a password: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input type="password" name="password" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> repeat password: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input type="password" name="passwordVerify" type="password" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> E-mail address: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="email" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> I'm a  </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <select id = "groupNo" name="groupNo" class="select2" >
           	                      <option value="1">Genernous Member</option>
           	                      <option value="2">Attractive Member</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> and I am a </div>
           	                <div class="col-2">
           	                  <div class="rowElem">
           	                    <select id="iam" name="iam" class="select2" >
           	                      <option value="1">Man</option>
           	                      <option value="0">Woman</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <div class="col-5 txt2"> Seeking a </div>
           	                <div class="col-4">
           	                  <div class="rowElem">
           	                    <p>
           	                      <select id="seek" name="seek" class="select3" >
           	                        <option value="0">Women</option>
           	                        <option value="1">Man</option>
                                  </select>
       	                        </p>
           	                    <p>&nbsp;</p>
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> My age is </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="age" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
<!--           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> My birthday: </div>
           	                <div class="col-6">
           	                  <div class="rowElem">
           	                    <select name="select2" class="select4" >
           	                      <option value="">01</option>
           	                      <option value="opt1">02</option>
           	                      <option value="opt1">03</option>
           	                      <option value="opt1">04</option>
           	                      <option value="opt1">05</option>
           	                      <option value="opt1">06</option>
           	                      <option value="opt1">07</option>
           	                      <option value="opt1">08</option>
           	                      <option value="opt1">09</option>
           	                      <option value="opt1">10</option>
           	                      <option value="opt1">11</option>
           	                      <option value="opt1">12</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <div class="col-6">
           	                  <div class="rowElem">
           	                    <select name="select2" class="select4" >
           	                      <option value="">01</option>
           	                      <option value="opt1">02</option>
           	                      <option value="opt1">03</option>
           	                      <option value="opt1">04</option>
           	                      <option value="opt1">05</option>
           	                      <option value="opt1">06</option>
           	                      <option value="opt1">07</option>
           	                      <option value="opt1">08</option>
           	                      <option value="opt1">09</option>
           	                      <option value="opt1">10</option>
           	                      <option value="opt1">11</option>
           	                      <option value="opt1">12</option>
           	                      <option value="opt1">13</option>
           	                      <option value="opt1">14</option>
           	                      <option value="opt1">15</option>
           	                      <option value="opt1">16</option>
           	                      <option value="opt1">17</option>
           	                      <option value="opt1">18</option>
           	                      <option value="opt1">19</option>
           	                      <option value="opt1">20</option>
           	                      <option value="opt1">21</option>
           	                      <option value="opt1">22</option>
           	                      <option value="opt1">23</option>
           	                      <option value="opt1">24</option>
           	                      <option value="opt1">25</option>
           	                      <option value="opt1">26</option>
           	                      <option value="opt1">27</option>
           	                      <option value="opt1">28</option>
           	                      <option value="opt1">29</option>
           	                      <option value="opt1">30</option>
           	                      <option value="opt1">31</option>
       	                        </select>
       	                      </div>
       	                    </div>
           	                <div class="col-7">
           	                  <div class="rowElem">
           	                    <p>
           	                      <select name="select2" class="select5" >
           	                        <option value="">1960</option>
           	                        <option value="opt1">1961</option>
           	                        <option value="opt1">1962</option>
           	                        <option value="opt1">1963</option>
           	                        <option value="opt1">1964</option>
           	                        <option value="opt1">1965</option>
           	                        <option value="opt1">1966</option>
           	                        <option value="opt1">1967</option>
           	                        <option value="opt1">1968</option>
           	                        <option value="opt1">1969</option>
           	                        <option value="opt1">1970</option>
           	                        <option value="opt1">1971</option>
           	                        <option value="opt1">1972</option>
           	                        <option value="opt1">1973</option>
           	                        <option value="opt1">1974</option>
           	                        <option value="opt1">1975</option>
           	                        <option value="opt1">1976</option>
           	                        <option value="opt1">1977</option>
           	                        <option value="opt1">1978</option>
           	                        <option value="opt1">1979</option>
           	                        <option value="opt1">1980</option>
           	                        <option value="opt1">1981</option>
           	                        <option value="opt1">1982</option>
           	                        <option value="opt1">1983</option>
           	                        <option value="opt1">1984</option>
           	                        <option value="opt1">1985</option>
           	                        <option value="opt1">1986</option>
           	                        <option value="opt1">1987</option>
           	                        <option value="opt1">1988</option>
           	                        <option value="opt1">1989</option>
           	                        <option value="opt1">1990</option>
           	                        <option value="opt1">1991</option>
           	                        <option value="opt1">1992</option>
           	                        <option value="opt1">1993</option>
           	                        <option value="opt1">1994</option>
       	                          </select>
       	                        </p>
           	                    <p>&nbsp;</p>
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
-->
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Country </div>
           	                <div class="col-2">
           	                  <div class="rowElem">
           	                    <p>
           	                      <select name="select2" class="select" >
           	                        <option value="">United States</option>
           	                        <option value="opt1">Canada</option>
       	                          </select>
       	                        </p>
           	                    <p>&nbsp;</p>
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container1 row2">
           	                <div class="col-1 txt2"> Zip code: </div>
           	                <div class="col-2">
           	                  <div class="rowElem2">
           	                    <input name="zip" type="text" class="input" />
       	                      </div>
       	                    </div>
           	                <br class="clear" />
       	                  </div>
           	              <div class="container row2">
           	                <div class="col-8">
           	                  <input type="checkbox" class="input1" name="1" />
       	                    </div>
           	                <div class="col-9">
           	                  <p>I<a href="index-terms.php">Accept Terms of Use</a></p>
           	                  <p>&nbsp;</p>
           	                  <p>&nbsp;</p>
           	                </div>
           	                <p>&nbsp;</p>
<p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;</p>
           	                <p>&nbsp;&nbsp;</p>
        <?php
          require_once('/home/worthyou/public_html/includes/classes/recaptchalib.php');
          $publickey = "6LfiL80SAAAAAEe-XdR9nStjbqgc_3mPbIM6Aq1L"; // you got this from the signup page
          echo recaptcha_get_html($publickey);
        ?>
           	                <div class="col-1">
                            	<a id="btnJoinNow" href="#" class="but-2">JOIN now</a>
                            </div>
       	                  </div>
       	                </fieldset>
       	              </form>


       	            </div>
       	          </div>
       	        </div>
           	    <p>&nbsp;</p>
              </div>
            </div>            
        </div>
    </div>
</section>
<footer>      
    <div class="main">
        <div class="inside">
            <div class="container">
                <div class="fleft">
                	<ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="index-search.php">Search</a></li>
                        <li><a href="index-join.php">Join</a></li>
                        <li><a href="index-blog.php">Blog</a></li>
                        <li><a href="index-faq.php">FAQs</a></li>
                        <li><a href="index-contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="fright"><span>Worth Your Time</span> &nbsp;&copy; 2011 &nbsp; &nbsp;<a href="index-privacy.php">Privacy policy</a> &nbsp;<!--{%FOOTER_LINK}--></div>     
            </div>   
        </div>
    </div>   
</footer>    

<script type="text/javascript">
		$(document).ready(function(){			
			//for prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
</script>
<!-- coded by Ann -->
</body>
</html>
