<? global $subTemplate; ?>
<? $maxPhoto = $subTemplate->getVars('maxPhoto'); ?>

<link href="style/style2.css" rel="stylesheet" type="text/css" />
<div id="if">   
<?php

	$img = isset($_GET['id']) ? $_GET['id'] : '';

	if ($img != ''){
		$normalImage = 'normal/'.$maxPhoto->normalPrefix . $img;
		$infoFile    = 'info/'.$img.'.info';
		
		$imageTitle = '';
		$imageDesc  = '';
		
		if (file_exists($infoFile)){
		      $imgData = file($infoFile);
		      $imageTitle = $imgData[0];
		      unset($imgData[0]);
		      foreach ($imgData as $value) {
		      	 $imageDesc .= $value;
		      }
		      
		      $imageDesc = htmlspecialchars($imageDesc);
		      $imageDesc = nl2br($imageDesc);
		      
		      
		}
		
		echo '<img src="'.$normalImage.'" alt="a" /><br/>';
		echo "<div id=\"imgInfo\"><h2>$imageTitle</h2><p>$imageDesc</p></div>";
	}
?>
</div>