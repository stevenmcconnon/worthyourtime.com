<?
	//pull in global template
	global $template;
	
	//Formatting functions for the registration form
	
	//If an error on the username field
	if($template->getVars('username') == 1) {
		$fieldUsername = "<span style='color: red'>* Username:</span>";
	}else{
		$fieldUsername = "Username:";
	}
	
	if($template->getVars('password') == 1) {
		$fieldPassword = "<span style='color: red'>* Password:</span>";
		$fieldPasswordV = "<span style='color: red'>* Verify Password:</span>";
		
	}else{
		$fieldPassword = "Password:";
		$fieldPasswordV = "Verify Password:";
		
	}
	
	if($template->getVars('email') == 1) {
		$fieldEmail = "<span style='color: red'>* Email:</span>";
	}else{
		$fieldEmail = "Email:";
	}
		
	if($template->getVars('age') == 1) {
		$fieldAge = "<span style='color: red'>* Age:</span>";
	}else{
		$fieldAge = "Age:";
	}
	
	if($template->getVars('iam') == 1) {
		$fieldIAM = "<span style='color: red'>* I am a:</span>";
	}else{
		$fieldIAM = "I am a:";
	}

	if($template->getVars('seek') == 1) {
		$fieldSeek = "<span style='color: red'>* Seeking a:</span>";
	}else{
		$fieldSeek = "Seeking a:";
	}
	
	if($template->getVars('zip') == 1) {
		$fieldZIP = "<span style='color: red'>* Zip:</span>";
	}else{
		$fieldZIP = "Zip:";
	}
	
	if($template->getVars('group') == 1) {
		$fieldGroup = "<span style='color: red'>* Group:</span>";
	}else{
		$fieldGroup = "Group:";
	}
	
?>