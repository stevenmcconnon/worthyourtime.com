<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	require_once('./includes/initilization.php');
	
	//Max fiels per user, hardcoded for now but can be dbed later.
	$maxPhotosPerUser = 5;
	$maxVidsPerUser = 5;
	
	$maxPhotoSize = 1024; // KB
	$maxVideoSize = 25000; //KB
	
	$photoExtensions = "bmp,jpg,jpeg,png";
	$videoExtensions = "wmv,mp4";
	
	$return = array();
	$return[0] = 0;
	
	//Form submitted POST vars
	$id = $_GET['id'];
	$type = $_GET['type'];
	$num = $_GET['num'];
	$info = $_GET['info'];
	$numMedia = $_GET['numMedia'];
	$flags = array();
	$i=0;

	
	//If processing an upload or prompting for an upload
	if($type == 'photo') {
		$path = _MEDIAROOT_.'photos/'.$id.'/';
	}else if($type == 'video'){
		$path = _MEDIAROOT_.'videos/'.$id.'/';
				//AMs can view viedeos at all times BUT GMs must be unlocked Or if your trying to view your own vids (which is allowed)
		if((($currentUser->getGroup() == '1' || !$currentUser->isLoggedIn())&& !$currentUser->checkPermissions($id,1) && $currentUser->getId() != $id)) {
			$return[1] = "Permission denied";
			die(json_encode($return));		
		}
	}else{
	
	}
	
	$fName = '';
	$count=0;
	
	if ($handle = @opendir($path)) {
		while (false !== ($entry = readdir($handle))) {
			$tmp = explode('_',$entry);
			$count++;
			//echo $path.$entry." ".$path.$num2.'_'.$tmp[1]." " . "<br>" . var_dump($locked) . "<br><br>";
			if ($tmp[0] == $num && $entry != '.' && $entry != '..' && !$fName) {
				$extension = substr(strrchr($tmp[0].'_'.$tmp[1], '.'), 1);
				if($info) {
					if($extension == 'info')
						$fName = $tmp[0].'_'.$tmp[1];
					else
						$fName = $tmp[0].'_'.$tmp[1].'.info';
				} else{
					if($extension != 'info')
						$fName = $tmp[0].'_'.$tmp[1];
				}
				
			}

		}
	
	}else{
		$return[1] = "Directory reading error";
		die(json_encode($return));	
	}
	
	if($numMedia) {
		$count=($count-2)/2;
		$return[0] =  1;
		$return[1]=$count;
		die(json_encode($return));
	}
	
	if(!$info) {
		//Still read info and get the MIME type
		$info = file_get_contents($path.$fName.".info");
		$infoArr = json_decode($info,true);
		$MIME = $infoArr['MIME'];
		
		if($type == "photo") {
			header('Content-type: '.$MIME);
		}else if ($type="video") {
			header('Content-type: '.$MIME);
		}
		
		header('Content-length: '.filesize($path.$fName));
		//header('Content-Disposition: attachment; filename="'.$num.'.'.$extension.'"');
		
		$chunk = 1024;
		$buffer = '';
		$fp = fopen($path.$fName, "r");
		while(!feof($fp)) {
			$buffer = fread($fp,$chunk);
			echo $buffer;
			ob_flush();
			flush();	
		}
				
		fclose($fp);
	
	}else{
		$contents = file_get_contents($path.$fName);
		$arr = json_decode($contents,true);
		$return[0] = 1;
		$return[1] = $arr;
		die(json_encode($return));
	}

			

	
?>
