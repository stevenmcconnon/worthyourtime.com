<?php
	define("_CWD_", getcwd());
	require_once('../includes/initilization.php');	
?>

<script src="ajaxContent/js/newOffer.js"></script>
<script src="js/superfish.js"></script>
<script>
$('#outerArea').superfish();
	$(document).ready(function(){
		$("#submit").button();
		
		$("#submit").click(function(){
		
			amount = $("#amount").val();
			
			newOffer("<?php echo $domain; ?>", "<?php echo $_GET['id']; ?>", amount, function(result){
				switch(result[0]) {
				case 0:
				  $("#outerArea").html("This is not a valid user to send an offer to.");
				  break;
				case 1:
				  $("#outerArea").html("Success! You offer was sent to this user.");
				  break;
				case 2:
				  $("#outerArea").html("This offer does not meet their required minimum. Please increase your offer and try again.");
				  break;
				case 3:
				  $("#outerArea").html("You have already sent an offer to this user.");
				  break;
				default:
				  $("#outerArea").html("Generic Error 13124. Contact Admin please.");
				}
			});
		});
	});
</script>

<div id="outerArea" style="margin: 0 auto; text-align: center;">
	<label for="amount">Amount: </label>
	<select id="amount" name="amount">
	  <option value="10">$10.00</option>
	  <option value="25">$25.00</option>
	  <option value="50">$50.00</option>
	  <option value="75">$75.00</option>
	  <option value="100">$100.00</option>
	  <option value="125">$125.00</option>
	  <option value="150">$150.00</option>
	  <option value="175">$175.00</option>
	  <option value="200">$200.00</option>
	  <option value="225">$225.00</option>
	  <option value="250">$250.00</option>
	  <option value="275">$275.00</option>
	  <option value="300">$300.00</option>
	  <option value="325">$325.00</option>
	  <option value="350">$350.00</option>
	  <option value="375">$375.00</option>
	  <option value="400">$400.00</option>
	  <option value="425">$425.00</option>
	  <option value="450">$450.00</option>
	  <option value="475">$475.00</option>
	  <option value="500">$500.00</option>
	  <option value="550">$550.00</option>
	  <option value="600">$600.00</option>
	  <option value="650">$650.00</option>
	  <option value="700">$700.00</option>
	  <option value="750">$750.00</option>
	  <option value="800">$800.00</option>
	  <option value="850">$850.00</option>
	  <option value="900">$900.00</option>
	  <option value="950">$950.00</option>
	  <option value="1000">$1,000.00</option>
	  <option value="1050">$1,050.00</option>
	  <option value="1100">$1,100.00</option>
	  <option value="1150">$1,150.00</option>
	  <option value="1200">$1,200.00</option>
	  <option value="1250">$1,250.00</option>
	  <option value="1300">$1,300.00</option>
	  <option value="1350">$1,350.00</option>
	  <option value="1400">$1,400.00</option>
	  <option value="1450">$1,450.00</option>
	  <option value="1500">$1,500.00</option>
	  <option value="1550">$1,550.00</option>
	  <option value="1600">$1,600.00</option>
	  <option value="1650">$1,650.00</option>
	  <option value="1700">$1,700.00</option>
	  <option value="1800">$1,800.00</option>
	  <option value="1900">$1,900.00</option>
	  <option value="2000">$2,000.00</option>
	  <option value="2500">$2,500.00</option>
	  <option value="3000">$3,000.00</option>
	  <option value="3500">$3,500.00</option>
	  <option value="4000">$4,000.00</option>
	  <option value="4500">$4,500.00</option>
	  <option value="5000">$5,000.00</option>
	  <option value="5500">$5,500.00</option>
	  <option value="6000">$6,000.00</option>
	  <option value="6500">$6,500.00</option>
	  <option value="7000">$7,000.00</option>
	  <option value="7500">$7,500.00</option>
	  <option value="10000">$10,000.00</option>
	  <option value="20000">$20,000.00</option>
	  <option value="30000">$30,000.00</option>
	  <option value="40000">$40,000.00</option>
	  <option value="50000">$50,000.00</option>
	</select>
	<br/><br/>
	<input type="submit" id="submit">
	
</div>
