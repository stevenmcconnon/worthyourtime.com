<?php
	define("_CWD_", getcwd());
	require_once('../includes/initilization.php');	
?>

<script src="ajaxContent/js/bestChance.js"></script>
<script src="js/superfish.js"></script>
<script>
$('#outerArea').superfish();
	$(document).ready(function(){
		newOffer("<?php echo $domain; ?>", "<?php echo $_GET['id']; ?>", function(result){
			switch(result[0]) {
			case 0:
			  $("#outerArea").html("This is not a valid user to send a best chance offer to.");
			  break;
			case 1:
			  $("#outerArea").html("Success! You offer was sent to this user.");
			  break;
			case 2:
			  $("#outerArea").html("You do not have enough credits for this. Please buy more credits and try again.");
			  break;
			case 3:
			  $("#outerArea").html("You have already sent an offer to this user.");
			  break;
			default:
			  $("#outerArea").html("Generic Error 13134. Contact Admin please.");
			}
		});
		
	});
</script>

<div id="outerArea" style="margin: 0 auto; text-align: center;">
	<!-- This is where our content goes -->
</div>
