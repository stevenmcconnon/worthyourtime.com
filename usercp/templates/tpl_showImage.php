<? global $maxPhoto; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <link href="http://dime106.dizinc.com/~ownage/sugar/includes/plugins/photo/style/style2.css" rel="stylesheet" type="text/css" />
   <title> </title>
</head>
<body id="if">   
<?php

	$img = isset($_GET['id']) ? $_GET['id'] : '';

	if ($img != ''){
		$normalImage = 'normal/'.$maxPhoto->normalPrefix . $img;
		$infoFile    = 'info/'.$img.'.info';
		
		$imageTitle = '';
		$imageDesc  = '';
		
		if (file_exists($infoFile)){
		      $imgData = file($infoFile);
		      $imageTitle = $imgData[0];
		      unset($imgData[0]);
		      foreach ($imgData as $value) {
		      	 $imageDesc .= $value;
		      }
		      
		      $imageDesc = htmlspecialchars($imageDesc);
		      $imageDesc = nl2br($imageDesc);
		      
		      
		}
		
		echo '<img src="'.$normalImage.'" alt="a" /><br/>';
		echo "<div id=\"imgInfo\"><h2>$imageTitle</h2><p>$imageDesc</p></div>";
	}
?>
</body>