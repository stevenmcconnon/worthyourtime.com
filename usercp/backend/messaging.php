<?
	if(!defined("_ACCESS_MESSAGING_FUNCTIONS_")) { //If were NOT including this from another script
		//Gain access to global variables and classes.  Start MySQLi and SESSION
		define("_CWD_", getcwd());
		require_once('../../includes/initilization.php');
		
		//Max fiels per user, hardcoded for now but can be dbed later.
			
		$rate = .15;
		
		//Form submitted POST vars
		$m = $_GET['m'];
		$do = $_GET['do'];
		
		$flags = array();
		$i=0;
		
		$return = array();
		$return[0] = 0;
		//If we are not logged in redirect to the login page
		//if(!$currentUser->isLoggedIn() || !$currentUser->CSRFValidate($_POST['token'])) {
		if(!$currentUser->isLoggedIn()) {
			$return[1] = "Permission Denied";
			die(json_encode($return));
		}
		
		$offerId = $_GET['offerId'];
		$submit = $_POST['submit'];
		$id = $_GET['id'];
		$mId = $_GET['mId'];
		$tId = $_GET['tId'];
		$type = $_GET['type'];
		$mType = $_GET['mType'];
		
		$subject = htmlentities($_POST['subject'], ENT_QUOTES);
		$body = htmlentities($_POST['body'], ENT_QUOTES);
				
		switch($type) {
			case "new":
				new_message($currentUser, $id, $subject, $body);
				break;
			
			case "reply":
				reply($currentUser, $mId, $subject, $body);
				break;
				
			case "view":
				view_message($currentUser,$mId,true);
			break;
			
			case "preview":
				view_message($currentUser,$mId,false);
			break;
			
			case "delete":
				delete_message($currentUser,$mId,$tId);
			break;
			
			case "enum":
				enum_messages($currentUser, $mType);
			break;
			default:
				$return[1] = "Invalid Request";
				die(json_encode($return));	
			break;
		}
	}
	/*
	reply
	reply
	reply
	
	Replies to an existing message
	*/
	function reply($currentUser, $mId, $subject, $body) {
		global $mysqli;
		
		$return[0] = 0;
		
		if(!$mId) {
			$return[1] = "Invalid Message Id";
			die(json_encode($return));			
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`tid` FROM `messaging` WHERE id=? ORDER BY `tid` DESC LIMIT 1");
		$stmt->bind_param('i',$mId);
		$stmt->execute();
		$stmt->bind_result($db_to, $db_from, $db_tid);
		$stmt->fetch();
		$stmt->close();
		
		//If the mid does not exist (count == 0)
		if(!$db_tid && !$db_from && !$db_to) {
			$return[1] = "Invalid Message Id";
			die(json_encode($return));				
		}
		
		//Determine who were sending this thread to
		$realTo = $db_to;
		$realFrom = $db_from;
		if($db_to == $currentUser->getId()) {
			$realTo = $db_from;
			$realFrom = $db_to;
		}

		//AMs can Message all day while GMs must accept and unlock an offer aka have permission
		if($currentUser->getGroup() == '1' && !$currentUser->checkPermissions($realTo,0)) {
			$return[1] = "Permission denied";
			die(json_encode($return));		
		}
		
		$stmt = $mysqli->prepare("SELECT `group` FROM `members` WHERE id=? LIMIT 1");
		$stmt->bind_param('s',$realTo);
		$stmt->execute();
		$stmt->bind_result($db_group);
		$stmt->fetch();
		$stmt->close();
		
		//AMs can only message GMs vica-versa aka their groups can not be the same OR the user is trying to send to a nonexistant ID
		if($currentUser->getGroup() == $db_group || !$db_group) {
			$return[1] = "Invalid Request";
			die(json_encode($return));			
		}
		
		$newTid = $db_tid + 1;
		
		$stmt = $mysqli->prepare("INSERT INTO `messaging` (`id`, `from`,`to`,`tid`,`subject`,`body`,`new`,`toDelete`,`fromDelete`,`time`) VALUES (?,?,?,?,?,?,1,0,0,UNIX_TIMESTAMP(NOW()))");
		$stmt->bind_param('iiiiss', $mId, $realFrom, $realTo, $newTid, $subject, $body);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	new message
	new message
	new message
	
	Creates a new thread (message)
	*/
	function new_message($currentUser, $toId, $subject, $body) {
		global $mysqli;
		
		$return[0] = 0;
		
		//AMs can Message all day while GMs must accept and unlock an offer aka have permission
		if(!defined("_ACCESS_MESSAGING_FUNCTIONS_")) { //If we are not accessing this from a script, if we are we can send messages to anyone
			if($currentUser->getGroup() == '1' && !$currentUser->checkPermissions($toId,0)) {
				$return[1] = "Permission denied";
				die(json_encode($return));		
			}
		}
		
		$stmt = $mysqli->prepare("SELECT `group` FROM `members` WHERE id=? LIMIT 1");
		$stmt->bind_param('s',$toId);
		$stmt->execute();
		$stmt->bind_result($db_group);
		$stmt->fetch();
		$stmt->close();
		
		//AMs can only message GMs vica-versa aka their groups can not be the same OR the user is trying to send to a nonexistant ID
		if($currentUser->getGroup() == $db_group || !$db_group) {
			$return[1] = "Invalid Request";
			die(json_encode($return));			
		}
		
		
		$stmt = $mysqli->prepare("INSERT INTO `messaging` (`from`,`to`,`tid`,`subject`,`body`,`new`,`toDelete`,`fromDelete`,`time`) VALUES (?,?,0,?,?,1,0,0,UNIX_TIMESTAMP(NOW()))");
		$stmt->bind_param('iiss',$currentUser->getId(), $toId, $subject, $body);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
			
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	view message
	view message
	view message
	
	Views the message
	*/
	function view_message($currentUser,$mId,$p) {
		global $mysqli;
		
		$return[0] = 0;
		
		if(!$mId) {
			$return[1] = "Invalid Message Id";
			die(json_encode($return));			
		}
		
		$array = array();
		$qarray = array();
		
		$array['mId'] = $mId;
		
		$stmt = $mysqli->prepare("SELECT `id`,`from`,`to`,`tid`,`subject`,`body`,`new`,`toDelete`,`fromDelete`,`time` FROM `messaging` WHERE id=?");
		$stmt->bind_param('i',$mId);
		$stmt->execute();
		$stmt->bind_result($db_id,$db_from,$db_to,$db_tid,$db_subject,$db_body,$db_new,$db_toDelete,$db_fromDelete,$db_time);
		
		while($stmt->fetch()) {
		
			if($currentUser->getId() == $db_to) {
				$isDeleted = $db_toDelete; //Grab the "to" portion of the delete string
			}else if($currentUser->getId() == $db_from) {
				$isDeleted = $db_fromDelete; //Grab the "from" portion of the delete string
			}
		
			
			if(!$isDeleted) {
				$array[$db_tid]['id'] = $db_id;
				$array[$db_tid]['from'] = $db_from;
				$array[$db_tid]['to'] = $db_to;
				$array[$db_tid]['subject'] = $db_subject;
				$array[$db_tid]['time'] = $db_time;
				
				if($p)
					$array[$db_tid]['body'] = $db_body;
				$array[$db_tid]['new'] = $db_new;
				
				if($db_new && $db_to == $currentUser->getId()) {
					$newMid = $mysqli->real_escape_string($mId);
					$newTid = $mysqli->real_escape_string($db_tid);
					
					if($p)
						$qarray[] = "UPDATE `messaging` SET `new` = '0' WHERE `id` = ". $newMid ." && `tid` = ". $newTid. " LIMIT 1";
				}
			}
		}
		
		
		$stmt->close();
		
		foreach($qarray as $query) {
			//echo $query."<br>";
			$mysqli->query($query);
		}
		
		$return[0] = 1;
		$return[1] = $array;
		die(json_encode($return));	 
	}
	
	/*
	delete message
	delete message
	delete message
	
	Views the message
	*/
	function delete_message($currentUser,$mId,$tId) {
		global $mysqli;
		
		$return[0] = 0;
		
		if(!$mId || !($tId >= 0)) {
			$return[1] = "Invalid Message Id";
			die(json_encode($return));			
		}
		
		$array = array();

		$stmt = $mysqli->prepare("SELECT `from`,`to` FROM `messaging` WHERE id=? && tid=? LIMIT 1");
		$stmt->bind_param('ii',$mId,$tId);
		$stmt->execute();
		$stmt->bind_result($db_from,$db_to);
		$stmt->fetch();
		$stmt->close();

		if($currentUser->getId() == $db_to) {
			$stmt = $mysqli->prepare("UPDATE `messaging` SET `toDelete` = 1 WHERE `id` = ? && `tid` = ? LIMIT 1");
		}else if($currentUser->getId() == $db_from) {
			$stmt = $mysqli->prepare("UPDATE `messaging` SET `fromDelete` = 1 WHERE `id` = ? && `tid` = ? LIMIT 1");
		}else{
			$return[1] = "Invalid State";
			die(json_encode($return));	
		}
	
		$stmt->bind_param('ii', $mId,$tId);
		$stmt->execute();
		$stmt->close();
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
function enum_messages($currentUser, $mType) {

	global $mysqli;
	
	$return[0] = 0;
	
	if(!$_GET['start'])
		$_GET['start'] = 0;
		
	if(!$_GET['limit'] || $_GET['limit'] <= 0) {
		$_GET['limit'] = 300;
	}
	
	$array = array();
	if($mType) //Sent
		$stmt = $mysqli->prepare("SELECT t.id, t.tid, t.from, t.to, t.toDelete, t.fromDelete, t.subject, t.new, t.time FROM messaging t JOIN ( SELECT id, MAX(tid) AS maxtid FROM messaging WHERE fromDelete = 0 && `from` = ? GROUP BY id ) m ON t.id = m.id AND t.tid = m.maxtid ORDER BY t.id DESC LIMIT ?,?");
	else //Received
		$stmt = $mysqli->prepare("SELECT t.id, t.tid, t.from, t.to, t.toDelete, t.fromDelete, t.subject, t.new, t.time FROM messaging t JOIN ( SELECT id, MAX(tid) AS maxtid FROM messaging WHERE toDelete = 0 && `to` = ? GROUP BY id ) m ON t.id = m.id AND t.tid = m.maxtid ORDER BY t.id DESC LIMIT ?,?");
	
	
	$stmt->bind_param('iii',$currentUser->getId(),$_GET['start'], $_GET['limit']);
	$stmt->execute();
	$stmt->bind_result($db_mid,$db_tid,$db_from,$db_to,$db_toDelete,$db_fromDelete,$db_subject,$db_new,$db_time);
	
	
	while($stmt->fetch()) {
		$array[$db_mid]['tid'] = $db_tid;
		$array[$db_mid]['from'] = $db_from;
		$array[$db_mid]['to'] = $db_to;
		$array[$db_mid]['subject'] = $db_subject;
		$array[$db_mid]['new'] = $db_new;	
		$array[$db_mid]['time'] = $db_time;	
		
	}
	
	
	$stmt->close();
	
	
	
	$return[0] = 1;
	$return[1] = $array;
	die(json_encode($return));	 

}
	
?>