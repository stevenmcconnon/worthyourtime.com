<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	require_once('../../includes/initilization.php');
	
	//Max fiels per user, hardcoded for now but can be dbed later.
		
	$rate = .15;
	
	//Form submitted POST vars
	$m = $_GET['m'];
	$do = $_GET['do'];
	
	$flags = array();
	$i=0;
	
	$return = array();
	$return[0] = '0';
	//If we are not logged in redirect to the login page
	if(!$currentUser->isLoggedIn()) {
		$return[1] = "Permission Denied";
		die(json_encode($return));
	}
	
	
	$flags = array();
	$i=0;
	

	
	//Validate form
	
	switch ($_GET['do']) {
	case "updateProf":
		updateProf($currentUser);
	break;
	
	case "changeEmail":
		changeEmail($currentUser);
	break;
		
	case "changePassword":
		changePassword($currentUser);
	break;
	
	default:
		showProfInfo($currentUser);
	break;
	
	}
	
function updateProf($currentUser) {
	global $mysqli;
	
	$return = array();
	$errors = array();
	$return[0] = 0;
	
	//Form submitted POST vars
	$frmName = htmlentities($_POST['name'],ENT_QUOTES);
	$frmAge = intval($_POST['age']);
	$frmHeightF = intval($_POST['heightF']);
	$frmHeightI = intval($_POST['heightI']);
	$frmZip = htmlentities($_POST['zip'],ENT_QUOTES);
	$frmIAM = intval($_POST['iam']);
	$frmSeek = intval($_POST['seek']);
	$frmEthnicity = intval($_POST['ethnicity']);
	$frmBodyType = intval($_POST['bodyType']);
	$frmLookingFor = intval($_POST['lookingFor']);
	$frmDescription = htmlentities($_POST['description'],ENT_QUOTES);
	$frmEyeColor = intval($_POST['eyeColor']);
	$frmHairColor = intval($_POST['hairColor']);
	$frmChildren = intval($_POST['children']);
	$frmEducation = intval($_POST['education']);
	$frmReligion = intval($_POST['religion']);
	$frmSalary = intval($_POST['salary']);
	$frmSmoking = intval($_POST['smoking']);
	$frmDrinking = intval($_POST['drinking']);
	$frmOccupation = htmlentities($_POST['occupation'], ENT_QUOTES);
	
	$frmBestChance = intval($_POST['best_chance']);
	$frmMinOffer = intval($_POST['min_offer']);
	
		
	//Check if username is blank
	if (!$frmName) {
		$errors[] = 'name';
	}
	
	if (strlen($frmDescription) < 50 || strlen($frmDescription) > 2048) {
		$errors[] = 'description';
	}
	
	if (strlen($frmOccupation) < 1 || strlen($frmOccupation) > 256) {
		$errors[] = 'occupation';
	}
	//Height
	if ($frmHeightF < 0 || $frmHeightF > 7 || $frmHeightI < 0 || $frmHeightI > 11) {
		$errors[] = 'height must be between 1ft 1in and 7ft 11in';	
	}
	
	//Between 18 and 99 only
	if (!$frmAge || $frmAge < 18 || $frmAge > 99) {
		$errors[] = 'age must be between 18 and 99';	
	}

	//Man or Woman (1 or 2)
	if($frmIAM > 2 || $frmIAM < 1)
		$errors[] = 'iam';

	//Men or Women (1 or 2)
	if($frmSeek > 2 || $frmSeek < 1)
		$errors[] = 'seeking';

	//White, Black, Spanish, Indian, Arab, Asian, Other
	if($frmEthnicity > 7 || $frmEthnicity < 1)
		$errors[] = 'ethnicity';

	//Slim, muscular, athletic, a few extra pounds, fat
	if($frmBodyType > 5 || $frmBodyType < 1)
		$errors[] = 'bodyType';

	//Casual, Friends, Long Term
	if($frmLookingFor > 3 || $frmLookingFor < 1)
		$errors[] = 'lookingFor';

	if($frmEyeColor > 7 || $frmEyeColor < 1)
		$errors[] = 'eyeColor';

	if($frmHairColor > 6 || $frmHairColor < 1)
		$errors[] = 'hairColor';

	if($frmChildren > 2 || $frmChildren < 1)
		$errors[] = 'children';

	if($frmEducation > 5 || $frmEducation < 1)
		$errors[] = 'education';

	if($frmReligion > 25 || $frmReligion < 1)
		$errors[] = 'religion';

	if($frmSalary > 6 || $frmSalary < 1)
		$errors[] = 'salary';

	if($frmSmoking > 5 || $frmSmoking < 1)
		$errors[] = 'smoking';

	if($frmDrinking > 4 || $frmDrinking < 1)
		$errors[] = 'drinking';
		
	


	
	 //Grab the zip credentials from the DB
	 $stmt = $mysqli->prepare("SELECT COUNT(*) FROM zip_code WHERE zip_code=? LIMIT 1");
	 $stmt->bind_param('s',$frmZip);
	 $stmt->execute();
	 $stmt->bind_result($zip_count);
	 $stmt->fetch();
	 $stmt->close();
	 
	 //If the zip is invalid
	 if(intval($zip_count) == 0) {
		$errors[] = 'zip'; 
	 }
	 
	 //If any of the checks failed
	 if(sizeof($errors) > 0) {
		$return[1] = $errors;
		die(json_encode($return));
	 }
	 
	//women w\ best chance offer
	if($currentUser->getGroup() == '2') {
		if ($frmMinOffer < 1) {
			$errors[] = 'minOffer';	
		}
		
		if ($frmBestChance < 1) {
			$errors[] = 'bestChance';	
		}
		
		if(sizeof($errors) > 0) {
			$return[1] = $errors;
			die(json_encode($return));
	    }
		
		$stmt = $mysqli->prepare("UPDATE `profile` SET name=?, heightF=?, heightI=?, age=?, zip=?, iAm=?, seeking=?, lookingFor=?, bodyType=?, ethnicity=?, description=?, eyeColor=?, hairColor=?, children=?, education=?, religion=?, salary=?, smoking=?, drinking=?, occupation=?, min_offer=?, best_chance=? WHERE id=?");
		$stmt->bind_param('siiisiiiiisiiiiiiiisiii',$frmName, $frmHeightF, $frmHeightI, $frmAge, $frmZip, $frmIAM, $frmSeek, $frmLookingFor, $frmBodyType, $frmEthnicity, $frmDescription, $frmEyeColor, $frmHairColor, $frmChildren, $frmEducation, $frmReligion, $frmSalary, $frmSmoking, $frmDrinking, $frmOccupation, $frmMinOffer, $frmBestChance, $currentUser->getId());
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
	}else{
		$stmt = $mysqli->prepare("UPDATE `profile` SET name=?, heightF=?, heightI=?, age=?, zip=?, iAm=?, seeking=?, lookingFor=?, bodyType=?, ethnicity=?, description=?, eyeColor=?, hairColor=?, children=?, education=?, religion=?, salary=?, smoking=?, drinking=?, occupation=? WHERE id=?");
		$stmt->bind_param('siiisiiiiisiiiiiiiisi',$frmName, $frmHeightF, $frmHeightI, $frmAge, $frmZip, $frmIAM, $frmSeek, $frmLookingFor, $frmBodyType, $frmEthnicity, $frmDescription, $frmEyeColor, $frmHairColor, $frmChildren, $frmEducation, $frmReligion, $frmSalary, $frmSmoking, $frmDrinking, $frmOccupation, $currentUser->getId());
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
	}
	
	$return[0]=1;
	$return[1]="Success";
	die(json_encode($return));

}

function showProfInfo($currentUser) {	
	global $mysqli;
	
	$result = $mysqli->query("SELECT `id`, `name`, `heightF`, `heightI`, `age`, `zip`, `iAm`, `seeking`, `bodyType`, `ethnicity`, `lookingFor`, `description`, `eyeColor`, `hairColor`, `children`, `education`, `religion`, `salary`, `smoking`, `drinking`, `occupation`, `min_offer`, `best_chance` FROM `profile` WHERE id=".$currentUser->getId(). " LIMIT 1");
	$return[0] = 1;
	$return[1] = $result->fetch_assoc();
	die(json_encode($return));
}

function changeEmail($currentUser) {	
	global $mysqli;
	$return = array();
	$return[0]=0;
	
	$email = htmlentities($_POST['email'],ENT_QUOTES);
	if (!preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email)) {
		$return[1] = "Invalid Email";
		die(json_encode($return));
	}
	$stmt = $mysqli->prepare("UPDATE `members` SET email=? WHERE id=?");
	$stmt->bind_param('si',$email,$currentUser->getId());
	$stmt->execute();
	$stmt->fetch();
	$stmt->close();
	
	$return[0] = 1;
	$return[1] = "Success";
	die(json_encode($return));
}

function changePassword($currentUser) {	
	global $mysqli;
	$return = array();
	$return[0]=0;
	
	$frmPassword = $_POST['password'];
	
	$salt = mt_rand(0,getrandmax());
	$password = md5($salt.$frmPassword);

	if (!$frmPassword) {
		$return[1] = "Invalid Password";
		die(json_encode($return));
	}
	$stmt = $mysqli->prepare("UPDATE `members` SET password=?,salt=? WHERE id=?");
	$stmt->bind_param('sii',$password,$salt,$currentUser->getId());
	$stmt->execute();
	$stmt->fetch();
	$stmt->close();
	
	$return[0] = 1;
	$return[1] = "Success";
	die(json_encode($return));
}
	
?>
