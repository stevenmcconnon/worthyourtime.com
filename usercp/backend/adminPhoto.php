<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	require_once('../../includes/initilization.php');

	//Max fiels per user, hardcoded for now but can be dbed later.
	$maxPhotosPerUser = 5;
	$maxVidsPerUser = 5;
	
	
	$maxPhotoSize = 25000; // KB
	$maxVideoSize = 25000; //KB
	
	$photoExtensions = "bmp,jpg,jpeg,png";
	$videoExtensions = "wmv,mp4";
	
	
	//Form submitted POST vars
	$m = $_GET['m'];
	$do = $_GET['do'];
	
	$flags = array();
	$i=0;
	
	$locale = "usercp";
	
	$return = array();
	$return[0] = 0;
	
		//If we are not logged in redirect to the login page
	//!$currentUser->CSRFValidate($_POST['token'])
	if(!$currentUser->isLoggedIn()) {
			$return[1] = 'Permission Denied';
			die(json_encode($return));
	}
	
	//If processing an upload or prompting for an upload
	if($do == 'upload') {
	
		$photoSubmit = $_POST['photoSubmit'];
		$videoSubmit = $_POST['videoSubmit'];
		
		if($photoSubmit) {
			processUpload(1);
		}else if ($videoSubmit){
			processUpload(0);
		}else{
			$return[1] = 'Invalid Request';
			die(json_encode($return));
		}

	
	} else if ($do == "reorder") {
		$type = $_GET['type'];
		$num1 = $_GET['num1'];
		$num2 = $_GET['num2'];
		
		if(!isset($num1) || !isset($num2)) {
			$return[1] = 'Invalid Request';
			die(json_encode($return));
		}
		
		if($type == 'photo') {
			reorder($num1,$num2,1);
		}else{
			reorder($num1,$num2,0);
		}
	
	} else if ($do == "remove") {
		$type = $_GET['type'];
		$num = $_GET['num'];
		
		if(!isset($num)) {
			$return[1] = 'Invalid Request';
			die(json_encode($return));
		}
		
		if($type == 'photo') {
			remove($num,1);
		}else{
			reorder($num,0);
		}
		
	} else{
			$return[1] = 'Invalid Request';
			die(json_encode($return));
	}
	
	function remove($num,$type) {
		global $currentUser;
		global $maxPhotosPerUser;
		global $maxVidsPerUser;
		
		$locked = array();
		
		$return = array();
		$return[0] = 0;
		
		$flag = $maxPhotosPerUser; //Flag for shifting all subsequent files down one number
		
		if($type == 1)
			$path = _MEDIAROOT_.'photos/'.$currentUser->getId().'/';
		else
			$path = _MEDIAROOT_.'videos/'.$currentUser->getId().'/';

		//Get number of photos present in users folder, create if necessary
		if ($handle = @opendir($path)) {
			while (false !== ($entry = readdir($handle))) {
				$tmp = explode('_',$entry);
				
			    //echo $path.$entry." ".$path.$num2.'_'.$tmp[1]." " . "<br>" . var_dump($locked) . "<br><br>";
				if ($tmp[0] == $num && $entry != '.' && $entry != '..') {
					if(!unlink($path.$entry)) {
							$return[1] = 'Remove Fail';
							die(json_encode($return));			
					}
					$flag = $num;
				}
				
				//We deleted files with a lower number now lets shift these ones down
				if($tmp[0] > $num && $entry != '.' && $entry != '..') {
					if(!rename($path.$entry,$path.($tmp[0]-1).'_'.$tmp[1])) {
							$return[1] = 'Remove fail';
							die(json_encode($return));
					}
				}
			}
		}else{
			$return[1] = 'Dir Read Error';
			die(json_encode($return));
		}
		
			$return[0] = 1;
			$return[1] = 'Success';
			die(json_encode($return));
	}
	
	function editinfo() {
	
	}
	
	function reorder($num1,$num2,$type) {
		global $currentUser;
		
		$return = array();
		$locked = array();
		
		$return[0] = 0;
		
		if($type == 1)
			$path = _MEDIAROOT_.'photos/'.$currentUser->getId().'/';
		else
			$path = _MEDIAROOT_.'videos/'.$currentUser->getId().'/';

		if ($handle = @opendir($path)) {
			while (false != readdir($handle)) {
				$count++;
			}
			$count=($count-2)/2; //correct for .. & . AND duplicates (each as a .info file)
		}else{
			$return[1] = 'Directory Read Error';
			die(json_encode($return));
		}
		
		$count = $count - 1;
		if($num1 > $count || $num2 > $count || $num1 < 0 || $num2 < 0)
			die("0-Invalid Request");
			
		closedir($handle);
		
		//Get number of photos present in users folder, create if necessary
		if ($handle = @opendir($path)) {
			while (false !== ($entry = readdir($handle))) {
				$tmp = explode('_',$entry);
				
			    //echo $path.$entry." ".$path.$num2.'_'.$tmp[1]." " . "<br>" . var_dump($locked) . "<br><br>";
				if ($tmp[0] == $num1 && $entry != '.' && $entry != '..' && !in_array($tmp[0].'_'.$tmp[1],$locked)) {
					if(!rename($path.$entry,$path.$num2.'_'.$tmp[1])) {
						$return[1] = 'Rename Fail (top)';
						die(json_encode($return));
					}
					
					$locked[] = $num2.'_'.$tmp[1];
				}
				if ($tmp[0] == $num2 && $entry != '.' && $entry != '..' && !in_array($tmp[0].'_'.$tmp[1],$locked)) {
					if(!rename($path.$entry,$path.$num1.'_'.$tmp[1])) {
						$return[1] = 'Rename Fail';
						die(json_encode($return));
					}
					
					$locked[] = $num1.'_'.$tmp[1];
				}
			}
		}else{
			$return[1] = 'Dir read error';
			die(json_encode($return));
		}
			
			$return[0] = 1;
			$return[1] = 'Success';
			die(json_encode($return));
	}
	
	function processUpload($type) {
		global $currentUser;
		global $photoExtensions;
		global $videoExtensions;
		global $maxPhotoSize;
		global $maxVideoSize;
		global $maxPhotosPerUser;
		global $maxVidsPerUser;
		
		$return = array();
		$return[0]=0;
		
		if($type == 1) {
			$path = _MEDIAROOT_.'photos/'.$currentUser->getId().'/';
			$extensionsList = $photoExtensions;
			$maxSize = $maxPhotoSize;
			$maxFiles = $maxPhotosPerUser;
		} else{
			$path = _MEDIAROOT_.'videos/'.$currentUser->getId().'/';
			$extensionsList = $videoExtensions;
			$maxSize = $maxVideoSize;
			$maxFiles = $maxVidsPerUser;
		}

		$fileLoc = $_FILES['file']['tmp_name'];
		$MIME = $_FILES['file']['type'];
		$count == 0;
		$extension = substr(strrchr($_FILES['file']['name'], '.'), 1);
		$frmName = preg_replace("(\r|\n)",'',htmlentities($_POST['name'], ENT_QUOTES));
		$frmDescription = preg_replace("(\r|\n)",'',htmlentities($_POST['description'], ENT_QUOTES));
		
		//we convert posted files data from cURL data into these variables
		if($_POST['type']) {
			//$fileLoc = $_POST['tmp_name'];
			$MIME = $_POST['type'];
			$extension = substr(strrchr($_POST['name'], '.'), 1);
			$frmName = preg_replace("(\r|\n)",'',htmlentities($_POST['name1'], ENT_QUOTES));
		}
		
		//Check name and description
		if(!$frmName || strlen($frmName) > 50) {
			$return[1] = 'Check Name';
			die(json_encode($return));
		}
		
		if(strlen($frmDescription) > 1024) {
			$return[1] = 'Check Description';
			die(json_encode($return));
		}
		
		
		//Get number of photos present in users folder, create if necessary
		if ($handle = @opendir($path)) {
			while (false != readdir($handle)) {
				$count++;
			}
		
		if($type) //photos
		     $count=($count-2)/2; //correct for .. & . AND duplicates (each as a .info file)
		else //videos
		     $count=($count-2)/3; //correct for .. & . AND duplicates (each as a .info file and thumbnail .jpg)
		
		}else if (!@mkdir($path)){
			$return[1] = 'Media Access Error';
			die(json_encode($return));
		}
		
		if(!$count)
			$count = 0;
		
		//Check size and extension first
		if( filesize($fileLoc) > $maxSize*1024 ) {
			$return[1]="Only image files under ".$maxSize." KB are allowed";
			die(json_encode($return));
		} else if ( !in_array($extension, explode(',',$extensionsList)) ) {
			$return[1]="This is image is not in a valid format. " . $fileLoc . $MIME . $extension . $frmName . $test;
			die(json_encode($return));
		}
		
		//Upload away
		if($count >= $maxFiles) {
			$return[0] = 2;
			$return[1]="Upload Limit Reached";
			die(json_encode($return));
		} else {
			
			$fp = fopen($fileLoc,'r');
			$fName = filesize($fileLoc)>1024 ? md5(fread($fp,1024)) : md5(fread($fp,filesize($fileLoc))); 
			fclose($fp);
			$newName = $count.'_'.$fName;
			$result = move_uploaded_file($_FILES['file']['tmp_name'],$path.$newName.'.'.$extension);			
			$fp = fopen($path.$newName.'.'.$extension.'.info','w');
			$infoArr = array();
			$infoArr['name'] = $frmName;
			$infoArr['description'] = $frmDescription;
			$infoArr['MIME'] = $MIME;
			fwrite($fp,json_encode($infoArr));
			fclose($fp);
			if ($type != 1) { //if video
				GetThumbnailFileName($path.$newName.'.'.$extension,$path.$newName.'.'.$extension.'.jpg');
			}
		}
			
			if($result == true) {
				$return[0]=1;
				$return[1]="Success";
				die(json_encode($return));
			} else {
				$return[0]=0;
				$return[1]="Move File Failed: " . "move_uploaded_file(" . $fileLoc. ", " . $path.$newName. '.' .$extension . ")";
				die(json_encode($return));
			}

	}



/*
Thumbnail Extraction Function
*/
function GetThumbnailFileName($FileName, $outputFileName ,$ScreenShortSecond = 10) {
    $VDOLastModifiedDate = filemtime($FileName);
    $Thumbnail_FileName  = $outputFileName;

    if (!file_exists($Thumbnail_FileName)) {
        $FFMPEG_Command = sprintf(
            "ffmpeg -i \"%s\" -y -ss \"00:00:%02d\" -f image2 \"%s\" > /dev/null 2>&1",
            $FileName, 0 + $ScreenShortSecond, $Thumbnail_FileName
        );
        system($FFMPEG_Command);
    }

    if (!file_exists($Thumbnail_FileName))
        return null;

    return $Thumbnail_FileName;
}
	
?>
