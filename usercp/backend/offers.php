<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	require_once('../../includes/initilization.php');
	
	//Include messaging since we will be sending PMs
	define("_ACCESS_MESSAGING_FUNCTIONS_",1);
	
	//Max fiels per user, hardcoded for now but can be dbed later.
		
	$rate = .15;
	
	//Form submitted POST vars
	$m = $_GET['m'];
	$do = $_GET['do'];
	
	$flags = array();
	$i=0;
	
	$return = array();
	$return[0] = 0;
	//If we are not logged in redirect to the login page
	if(!$currentUser->isLoggedIn() && !$currentUser->CSRFValidate($_POST['token'])) {
		$return[1] = "Permission Denied";
		die(json_encode($return));
	}
	
	$offerId = $_GET['offerId'];
	$submit = $_POST['submit'];
	$id = $_GET['id'];
	$type = $_GET['type'];
			
	//Generous Users
	if($currentUser->getGroup() == '1') {

		switch($type) {
			case "new":
				$price = $_POST['price'];
				newOffer($currentUser, $id, $price);
			break;
			
			case "accept":
				accept_counter_offer($currentUser,$offerId);			
			break;
			
			case "bestChance":
				best_chance_offer($currentUser, $id);
			break;
			
			case "reject":
				reject_counter_offer($currentUser,$offerId);			
			break;
			
			case "rescind":
				rescind_offer($currentUser,$offerId);
			break;
			
			case "unlock":
				unlock_offer($currentUser, $offerId);
			break;
			
			case "view":
				view_offers($currentUser->getId());
				break;
				
			default:
				$return[1] = "Invalid Request";
				die(json_encode($return));	
			break;
		}
	//Attractive Memberssc
	}else if($currentUser->getGroup() == '2') {
		switch($type) {
			
			case "accept":
				accept_offer($currentUser,$offerId);
			break;
			
			case "reject":
				reject_offer($currentUser,$offerId);
			break;
			
			case "counter":
				counter_offer($currentUser,$offerId,$_POST['price']);
			break;
			
			case "rescind":
				rescind_offer($currentUser,$offerId);
			break;
			
			case "view":
				view_offers($currentUser->getId());
			break;
			
			default:
				$return[1] = "Invalid Request";
				die(json_encode($return));
			break;
		}	
	}
	
	/*
	NEW OFFER
	NEW OFFER
	NEW OFFER
	NEW OFFER
	A GU will make a new offer to an AM
	*/
	function newOffer($currentUser, $toId, $price) {
		global $mysqli;
		
		$fromId = $currentUser->getId();
		$return[0] = 0;
		
		$stmt = $mysqli->prepare("SELECT `to`.`id` , `to`.`group`, `to`.`email`, `pTo`.`min_offer`, COUNT(`offerRes`.`id`), `settingsRes`.`email`
									FROM `members` AS `to`

									LEFT JOIN `profile` AS pTo ON pTo.id = to.id
									LEFT JOIN `offers` AS offerRes ON offerRes.from = ? && offerRes.to = to.id
									LEFT JOIN `user_settings` AS settingsRes ON settingsRes.id = to.id

									WHERE to.id = ? LIMIT 1");
									
		$stmt->bind_param('ii',$currentUser->getId(), $toId);
		$stmt->execute();
		$stmt->bind_result($db_to, $db_group, $db_email, $db_min_offer, $db_offeredAlready, $db_emailSettings);
		$stmt->fetch();
		$stmt->close();
		
		
		//If trying to make an offer on someone other than an attractive member or a nonexistant user
		if($db_group != 2) {
			$return[1] = "Invalid user";
			die(json_encode($return));		
		}
		
		//If trying to make less than the attractive members minimum offer
		if($price < $db_min_offer) {
			$return[0] = 2;
			$return[1] = "Offer does not meet minimum";
			die(json_encode($return));		
		}
		
		//If a record exists in the DB for an offer FROM fromId TO toId
		if($db_offeredAlready) {
			$return[0] = 3;
			$return[1] = "An offer to this user has already been made";
			die(json_encode($return));			
		}
		
		$stmt = $mysqli->prepare("INSERT INTO `offers` (`from`,`to`,`type`,`status`,`amount`,`counter-amount`) VALUES (?,?,'reg','pending',?,0)");
		$stmt->bind_param('iii',$fromId, $toId, $price);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Send PM with offer details
		PMOfferStatus('offer_new.txt',$currentUser,intval($_POST['price']),$toId);
		
		//If the user has elected to receive mail
		if($db_emailSettings)
			EmailOfferStatus('offer_new', $mysqli, $currentUser, intval($_POST['price']), $toId);
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}

	/*
	BEST_CHANCE_OFFER
	BEST_CHANCE_OFFER
	BEST_CHANCE_OFFER
	BEST_CHANCE_OFFER
	
	A GU will make a best chance offer for an AM
	*/
	function best_chance_offer($currentUser, $toId) {
		global $mysqli;
		global $rate;
		
		$fromId = $currentUser->getId();
		$return[0] = 0;
		
		$stmt = $mysqli->prepare("SELECT `group`,`permissions` FROM `members` WHERE id=? LIMIT 1");
		$stmt->bind_param('s',$toId);
		$stmt->execute();
		$stmt->bind_result($db_group,$db_permissions);
		$stmt->fetch();
		$stmt->close();
		
		$stmt = $mysqli->prepare("SELECT `best_chance` FROM `profile` WHERE id=? LIMIT 1");
		$stmt->bind_param('s',$toId);
		$stmt->execute();
		$stmt->bind_result($db_best_offer);
		$stmt->fetch();
		$stmt->close();

		$stmt = $mysqli->prepare("SELECT `balance` FROM `credits` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $fromId);
		$stmt->execute();
		$stmt->bind_result($db_balance);
		$stmt->fetch();
		$stmt->close();
		
		//If trying to make an offer on someone other than an attractive member or a nonexistant user
		if($db_group != 2) {
			$return[1] = "Invalid user";
			die(json_encode($return));		
		}
		
		//If trying to make less than the attractive members minimum offer
		if(($rate*$db_best_offer) > $db_balance) {
			$return[0] = 2;
			$return[1] = "Insuficent credit balance";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("SELECT COUNT(*) FROM `offers` WHERE `from` = ? && `to` = ? LIMIT 1");
		$stmt->bind_param('ii', $fromId, $toId);
		$stmt->execute();
		$stmt->bind_result($db_offeredAlready);
		$stmt->fetch();
		$stmt->close();
		
		//If a record exists in the DB for an offer FROM fromId TO toId
		if($db_offeredAlready) {
			$return[0] = 3;
			$return[1] = "An offer to this user has already been made";
			die(json_encode($return));			
		}
		
		$stmt = $mysqli->prepare("INSERT INTO `offers` (`from`,`to`,`type`,`status`,`amount`,`counter-amount`) VALUES (?,?,'best','unlocked',?,0)");
		$stmt->bind_param('iii',$fromId, $toId, $db_best_offer);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Deduct credits
		$newBalance = $db_balance - $rate*$db_best_offer;
		$stmt = $mysqli->prepare("UPDATE `credits` SET `balance` = ? WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('ii',$newBalance, $fromId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Set user permissions, add full access to the AM
		if(!$db_permissions) { //first time
			$db_permissions = array();
		}else{
			$db_permissions = json_decode($db_permissions,true);
			
		}
		$db_permissions[$toId][0] = 1;
		$db_permissions[$toId][1] = 1;
		
		$db_permissions = json_encode($db_permissions);

		$stmt = $mysqli->prepare("UPDATE `members` SET `permissions` = ? WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('si',$db_permissions, $fromId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Send PM with offer details
		PMOfferStatus('offer_best_chance.txt',$currentUser,$db_best_offer,$toId);
				
		$stmt = $mysqli->prepare("SELECT `email` FROM `user_settings` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $toId);
		$stmt->execute();
		$stmt->bind_result($db_email);
		$stmt->fetch();
		$stmt->close();		
		
		//If the user has elected to receive mail
		if($db_email)
			EmailOfferStatus('offer_best_chance', $mysqli, $currentUser, $db_best_offer, $toId);
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	Reject_counter_offer
	Reject_counter_offer
	Reject_counter_offer
	Reject_counter_offer
	
	A GM can reject a counter offer a AM made
	*/
	function reject_counter_offer($currentUser,$offerId) {
		global $mysqli;
		
		$fromId = $currentUser->getId();
		
		$return[0] = 0;
		
		if(!$offerId) {
			$return[1] = "Invalid State";
			die(json_encode($return));
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,`counter-amount`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_counter_amount,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		//If trying to mess with someone else's offers
		if($db_from != $fromId || $db_count == 0 || $db_status != 'countered') {
			$return[1] = "Invalid State";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("UPDATE `offers` SET `status` = 'rejected' WHERE `id` = ?");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		//Send PM with offer details
		PMOfferStatus('offer_reject.txt',$currentUser,$db_counter_amount,$db_to);
		
		$stmt = $mysqli->prepare("SELECT `email` FROM `user_settings` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $db_to);
		$stmt->execute();
		$stmt->bind_result($db_email);
		$stmt->fetch();
		$stmt->close();		
		
		//If the user has elected to receive mail
		if($db_email)
			EmailOfferStatus('offer_reject', $mysqli, $currentUser, $db_counter_amount, $db_to);
		
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	Accept_counter_offer
	Accept_counter_offer
	Accept_counter_offer
	Accept_counter_offer
	
	An GM can accept a counter offer a AM made
	*/
	function accept_counter_offer($currentUser,$offerId) {
		global $mysqli;
		
		$fromId = $currentUser->getId();
		
		$return[0] = 0;
		
		if(!$offerId) {
			$return[1] = "Invalid State";
			die(json_encode($return));
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,`counter-amount`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_counter_amount,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		//If trying to mess with someone else's offers
		if($db_from != $fromId || $db_count == 0 || $db_status != 'countered') {
			$return[1] = "Invalid State";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("UPDATE `offers` SET `status` = 'accepted' WHERE `id` = ?");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		//Send PM with offer details
		PMOfferStatus('offer_counter_accept.txt',$currentUser,$db_counter_amount,$db_to);
		
		$stmt = $mysqli->prepare("SELECT `email` FROM `user_settings` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $db_to);
		$stmt->execute();
		$stmt->bind_result($db_email);
		$stmt->fetch();
		$stmt->close();		
		
		//If the user has elected to receive mail
		if($db_email)
			EmailOfferStatus('offer_counter_accept', $mysqli, $currentUser, $db_counter_amount, $db_from);
		
		//Attempt to unlock the offer, if the offer can't be unlocked it will remain in the accepted state until the user fulfills the credit balance
		unlock_offer($currentUser,$offerId);
			 
	}

	/*
	unlock_offer
	unlock_offer
	unlock_offer
	unlock_offer
	
	A GU will unlock an offer HE made that's in the accepted state
	*/
	function unlock_offer($currentUser, $offerId) {
		global $mysqli;
		global $rate;
		
		$fromId = $currentUser->getId();
		
		$return[0] = 0;
		
		$stmt = $mysqli->prepare("SELECT `group`,`permissions` FROM `members` WHERE id=? LIMIT 1");
		$stmt->bind_param('s',$fromId);
		$stmt->execute();
		$stmt->bind_result($db_group,$db_permissions);
		$stmt->fetch();
		$stmt->close();
		
		$stmt = $mysqli->prepare("SELECT `balance` FROM `credits` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $fromId);
		$stmt->execute();
		$stmt->bind_result($db_balance);
		$stmt->fetch();
		$stmt->close();
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,`amount`,`counter-amount`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_amount,$db_counter_amount,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		//If a record exists in the DB for an offer OR trying to mess with someone else's offer OR the offer isn't yet accepted
		echo $db_from. " " . $db_count . " " . $db_status;
		if($db_from != $fromId || $db_count == 0 || $db_status != 'accepted') {
			$return[1] = "Invalid state";
			die(json_encode($return));			
		}
		
		//Check credit balance
		$offerAmount = $db_amount;
		if($db_counter_amount > $db_amount)
			$offerAmount = $db_counter_amount;
			
		if(($rate*$offerAmount) > $db_balance) {
			$return[0] = 2;
			$return[1] = "Insuficent credit balance";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("UPDATE `offers` SET `status` = 'unlocked' WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i',$offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Deduct credits
		$newBalance = $db_balance - $rate*$offerAmount;
		$stmt = $mysqli->prepare("UPDATE `credits` SET `balance` = ? WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('ii',$newBalance, $fromId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Set user permissions, add full access to the AM
		if(!$db_permissions) { //first time
			$db_permissions = array();
		}else{
			$db_permissions = json_decode($db_permissions,true);
		}
		$db_permissions[$db_to][0] = 1;
		$db_permissions[$db_to][1] = 1;
		
		$db_permissions = json_encode($db_permissions);

		$stmt = $mysqli->prepare("UPDATE `members` SET `permissions` = ? WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('si',$db_permissions, $fromId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	Reject_Offer
	Reject_Offer
	Reject_Offer
	Reject_Offer
	An AM can reject an offer a GM made
	*/
	function reject_offer($currentUser,$offerId) {
		global $mysqli;
		
		$toId = $currentUser->getId();
		
		$return[0] = 0;
		
		if(!$offerId) {
			$return[1] = "Invalid State";
			die(json_encode($return));
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,`amount`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_amount,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		//If trying to mess with someone else's offers
		if($db_to != $toId || $db_count == 0 || $db_status != 'pending') {
			$return[1] = "Invalid State";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("UPDATE `offers` SET `status` = 'rejected' WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		//Send PM with offer details
		PMOfferStatus('offer_reject.txt',$currentUser,$db_amount,$db_from);
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	Accept_offer
	Accept_offer
	Accept_offer
	Accept_offer
	
	An AM can accept an offer a GM made
	*/
	function accept_offer($currentUser,$offerId) {
		global $mysqli;
		
		$toId = $currentUser->getId();
		
		$return[0] = 0;
		
		if(!$offerId) {
			$return[1] = "Invalid State";
			die(json_encode($return));
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,`amount`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_amount,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		
		//If trying to mess with someone else's offers
		if($db_to != $toId || $db_count == 0 || $db_status != 'pending') {
			$return[1] = "Invalid State";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("UPDATE `offers` SET `status` = 'accepted' WHERE `id` = ?");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Send PM with offer details
		PMOfferStatus('offer_accept.txt',$currentUser,$db_amount,$db_from);
		
		$stmt = $mysqli->prepare("SELECT `email` FROM `user_settings` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $db_from);
		$stmt->execute();
		$stmt->bind_result($db_email);
		$stmt->fetch();
		$stmt->close();		
		
		//If the user has elected to receive mail
		if($db_email)
			EmailOfferStatus('offer_accept', $mysqli, $currentUser, $db_amount, $db_from);
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	Counter_Offer
	Counter_Offer
	Counter_Offer
	Counter_Offer
	
	An AM can counter an offer a GM made
	*/
	function counter_offer($currentUser,$offerId,$price) {
		global $mysqli;
		
		$toId = $currentUser->getId();
		
		$return[0] = 0;
		
		if(!$offerId) {
			$return[1] = "Invalid State";
			die(json_encode($return));
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,`amount`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_amount,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		//If trying to mess with someone else's offers
		if($db_to != $toId || $db_count == 0 || $db_status != 'pending' || $price <= $db_amount) {
			$return[1] = "Invalid State";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("UPDATE `offers` SET `status` = 'countered', `counter-amount` = ? WHERE `id` = ?");
		$stmt->bind_param('ii', $price ,$offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		//Send PM with offer details
		PMOfferStatus('offer_counter.txt',$currentUser,intval($price),$db_from);
		
		$stmt = $mysqli->prepare("SELECT `email` FROM `user_settings` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $db_to);
		$stmt->execute();
		$stmt->bind_result($db_email);
		$stmt->fetch();
		$stmt->close();		
		
		//If the user has elected to receive mail
		if($db_email)
			EmailOfferStatus('offer_counter', $mysqli, $currentUser, 0, $db_to);
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	/*
	rescind_offer
	rescind_offer
	rescind_offer
	rescind_offer
	
	An AM can recind and offer
	*/
	function rescind_offer($currentUser,$offerId) {
		global $mysqli;
		
		$return[0] = 0;
		
		if(!$offerId) {
			$return[1] = "Invalid State";
			die(json_encode($return));
		}
		
		$stmt = $mysqli->prepare("SELECT `to`,`from`,`status`,COUNT(*) FROM `offers` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->bind_result($db_to,$db_from,$db_status,$db_count);
		$stmt->fetch();
		$stmt->close();
		
		//If trying to mess with someone else's offers OR trying to mess with an  offer in the incorrect state
		if(($currentUser->getGroup() == '1' && $db_status != 'pending') || ($currentUser->getGroup() == '2' && $db_status != 'countered')
			|| ($db_to != $currentUser->getId() && $db_from != $currentUser->getId())) {
			$return[1] = "Invalid State";
			die(json_encode($return));		
		}
		
		
		$stmt = $mysqli->prepare("DELETE FROM `offers` WHERE `id` = ?");
		$stmt->bind_param('i', $offerId);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		//Send PM AND maybe email with offer details
		if($currentUser->getId() == $db_to) {
			PMOfferStatus('offer_recind.txt',$currentUser,0,$db_from);
			$mailTo = $db_from;
		}else{
			PMOfferStatus('offer_recind.txt',$currentUser,0,$db_to);
			$mailTo = $db_to;
		}
		
		$stmt = $mysqli->prepare("SELECT `email` FROM `user_settings` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $mailTo);
		$stmt->execute();
		$stmt->bind_result($db_email);
		$stmt->fetch();
		$stmt->close();		
		
		//If the user has elected to receive mail
		if($db_email)
			EmailOfferStatus('offer_rescind', $mysqli, $currentUser, 0, $mailTo);
		
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));	 
	}
	
	function view_offers($id) {
		global $mysqli;
		
		$return[0] = 0;
		$defaultFlag = 0;
		$limitFlag = 0;
		
		$limitStr = '';
		
		$status = $_GET['status'];
		$limit = $_GET['limit'];
		$oType = $_GET['oType'];
		
		switch ($oType) {
		
		case "sent":
			$prepStr = 'i';
			//$select = "SELECT `id`,`to`,`from`,`status`,`type`,`amount`,`counter-amount` FROM `offers` WHERE `from` = ?";
			$select = "
			SELECT
    			 offers.id,
    			 offers.from,
    			 from_member.username AS fromUsername,
    			 offers.to,
    			 to_member.username AS toUsername,
    			 offers.type,
    			 offers.status,
    			 offers.amount,
    			 offers.`counter-amount`
			FROM offers 
			JOIN members AS from_member ON from_member.id = offers.from
			JOIN members AS to_member ON to_member.id = offers.to
			WHERE offers.from = ?";
		break;
		case "received":
			$prepStr = 'i';
			//$select = "SELECT `id`,`to`,`from`,`status`,`type`,`amount`,`counter-amount` FROM `offers` WHERE `to` = ?";
			$select = "
			SELECT
    			 offers.id,
    			 offers.from,
    			 from_member.username AS fromUsername,
    			 offers.to,
    			 to_member.username AS toUsername,
    			 offers.type,
    			 offers.status,
    			 offers.amount,
    			 offers.`counter-amount`
			FROM offers 
			JOIN members AS from_member ON from_member.id = offers.from
			JOIN members AS to_member ON to_member.id = offers.to
			WHERE offers.to = ?";

		break;
		default:
			$prepStr = 'ii';
			$defaultFlag = 1;
			//$select = "SELECT `id`,`to`,`from`,`status`,`type`,`amount`,`counter-amount` FROM `offers` WHERE `from` = ? || `to` = ?";
			$select = "
			SELECT
    			 offers.id,
    			 offers.from,
    			 from_member.username AS fromUsername,
    			 offers.to,
    			 to_member.username AS toUsername,
    			 offers.type,
    			 offers.status,
    			 offers.amount,
    			 offers.`counter-amount`
			FROM offers 
			JOIN members AS from_member ON from_member.id = offers.from
			JOIN members AS to_member ON to_member.id = offers.to
			WHERE offers.from = ? || offers.to = ?";

		break;
		}
		
		if($limit) {
			$prepStr = $prepStr.'i';
			$limitFlag = 1;
		}
		
		if($defaultFlag) {
			if($limitFlag) {
				$stmt = $mysqli->prepare($select." LIMIT ?");
				$stmt->bind_param('iii', $id,$id,$limit);
			}else{
				$stmt = $mysqli->prepare($select);
				$stmt->bind_param('ii', $id, $id);
			}
		}else{
			if($limitFlag) {
				$stmt = $mysqli->prepare($select." LIMIT ?");
				$stmt->bind_param('ii', $id,$limit);
			}else{
				$stmt = $mysqli->prepare($select);
				$stmt->bind_param('i', $id);
			}
		}

		$stmt->execute();
		$stmt->bind_result($db_id,$db_from,$db_fromUser,$db_to,$db_toUser,$db_type,$db_status,$db_amount,$db_counter);
		
		$array = array();
		$i=0;
		
		while($stmt->fetch()) {
			$array[$db_id]['to'] = $db_to;
			$array[$db_id]['toUser'] = $db_toUser;
			$array[$db_id]['from'] = $db_from;
			$array[$db_id]['fromUser'] = $db_fromUser;
			$array[$db_id]['status'] = $db_status;
			$array[$db_id]['type'] = $db_type;
			$array[$db_id]['amount'] = $db_amount;
			$array[$db_id]['counter-amount'] = $db_counter;
		}
		
		
		$stmt->close();
		
		$return[0]=1;
		$return[1] = $array;
		die(json_encode($return));	 
	}
	
	function PMOfferStatus($predefined,$currentUser,$amount,$to) {
		$message = json_decode(file_get_contents(_PREDEFINEDS_.$predefined),true);
		
		$patterns = array();
		$replace = array();
		
		$replace[] = $currentUser->getUsername();
		$replace[] = $amount;
		$replace[] = $amount*.15;
		
		$patterns[] = "/\{username\}/";
		$patterns[] = "/\{amount\}/";
		$patterns[] = "/\{credits\}/";
		

		foreach ($message as $key => $value) {
			$message[$key] = preg_replace($patterns, $replace ,$message[$key]);
		}
		
		//Send PM
		require_once('./messaging.php');
		new_message($currentUser,$to,$message['subject'],$message['body'],false);
		
	}
	
	function EmailOfferStatus($predefined, $mysqli, $currentUser, $amount, $to) {
		
		$stmt = $mysqli->prepare("SELECT `username`,`email` FROM `members` WHERE `id` = ? LIMIT 1");
		$stmt->bind_param('i', $to);
		$stmt->execute();
		$stmt->bind_result($db_username,$db_email);
		$stmt->fetch();
		$stmt->close();	
		
		$html = json_decode(file_get_contents(_PREDEFINEDS_.'email/'.$predefined.'.htm'),true);
		$plainText = json_decode(file_get_contents(_PREDEFINEDS_.'email/'.$predefined.'.txt'),true);
		
		$patterns = array();
		$replace = array();
		
		$replace[] = $currentUser->getUsername();
		$replace[] = $db_username;
		$replace[] = $amount;
		$replace[] = $amount*.15;
		
		$patterns[] = "/\{fromUsername\}/";
		$patterns[] = "/\{toUsername\}/";
		$patterns[] = "/\{amount\}/";
		$patterns[] = "/\{credits\}/";
		

		foreach ($plainText as $key => $value) {
			$html[$key] = preg_replace($patterns, $replace ,$html[$key]);
			$plainText[$key] = preg_replace($patterns, $replace ,$plainText[$key]);
			
		}
		
		
		require_once('../../includes/classes/class.phpmailer.php');

		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
		$mail->IsSMTP();
		$mail->Host = "localhost";
		$mail->AddReplyTo('no-reply@worthyourtime.com', 'Worthyourtime.com');
		$mail->SetFrom('no-reply@worthyourtime.com', 'Worthyourtime.com');
		$mail->AddAddress($db_email);
		$mail->Subject = $plainText['subject'];
		$mail->AltBody = $plainText['body']; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML($html['body']);
		$mail->Send();

	}
?>
