<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	
	require_once('../includes/initilization.php');
	
	global $installedLoc;
	
	//global $template;
	
	//Form submitted POST vars
	$m = $_GET['m'];
	
	$flags = array();
	$i=0;
	
	$locale = "usercp";
	
	//If we are not logged in redirect to the login page
	if(!$currentUser->isLoggedIn()) {
		$template->base_page("usercp");
		$template->terminate(1);
		$template->redirect("login");
		$template->render();
	}
	
	
	require_once($installedLoc."includes/plugins/photo/general/maxImageUpload.class.php");
	
	global $maxPhoto;
	$maxPhoto = new maxImageUpload($currentUser->getId());
	require_once("./templates/tpl_showImage.php");

	
?>