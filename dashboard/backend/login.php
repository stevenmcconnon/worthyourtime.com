<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	
	define("_CWD_", getcwd());
	require_once('../includes/initilization.php');
	
	//Form submitted POST vars
	$frmUsername = $_POST['username'];
	$frmPassword = $_POST['password'];
	$do = $_GET['do'];
	
	$return[0] = 0;
	
	//If logging out
	if($do == "logout") {
		session_destroy();
		$return[0] = 1;
		$return[1] = "Success";
		die(json_encode($return));
	}
	
	//Check form data
	if(!isset($frmUsername) || !isset($frmPassword)) {
		$return[1] = "Invalid Request";
		die(json_encode($return));
	}
	
     //Grab the user credentials from the DB
     $stmt = $mysqli->prepare("SELECT `id`,`salt`,`password`,`group` FROM members WHERE username=? LIMIT 1");
     $stmt->bind_param('s',$frmUsername);
     $stmt->execute();
     $stmt->bind_result($db_id,$db_salt,$db_password,$db_group);
     $stmt->fetch();
     $stmt->close();

	//Check login credentials
	if (!$db_id) {
		$return[1] = "Login Fail";
		die(json_encode($return));
	}
	
	if(md5($db_salt.$frmPassword) != $db_password) {
		$return[1] = "Login Fail";
		die(json_encode($return));
	}
	
	//If we have not hit any errors that caused login to fail
	
	$_SESSION['id'] = $db_id;
	$_SESSION['group'] = $db_group;
	$_SESSION['username'] = $frmUsername;
	
	 //Grab the user credentials from the DB
     $stmt = $mysqli->prepare("UPDATE `members` SET `last_ip` = ? WHERE id=? LIMIT 1");
     $stmt->bind_param('si',$_SERVER['REMOTE_ADDR'],$db_id);
     $stmt->execute();
     $stmt->close();
	 
	 $return[0]=1;
	 $return[1] = "Success";
	 
	 die(json_encode($return));
?>