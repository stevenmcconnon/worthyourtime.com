<?

	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	define("_LEGACY_",1);
	require_once('../includes/initilization.php');
	require_once('../includes/classes/zipcode.class.php');
	require_once('../includes/classes/queue.class.php');
	
	//Max fiels per user, hardcoded for now but can be dbed later.
		
	$rate = .15;
	
	//Form submitted POST vars
	$m = $_GET['m'];
	$do = $_GET['do'];
	
	$flags = array();
	$i=0;
	
	$return = array();
	$return[0] = '0';
	//If we are not logged in redirect to the login page
	if(!$currentUser->isLoggedIn()) {
		$return[1] = "Permission Denied";
		die(json_encode($return));
	}
	

	viewProfile($currentUser);
	
function viewProfile($currentUser) {

	global $mysqli;
	
	$id = $_GET['id'];
	
	$return[0] = 0;

	$stmt = $mysqli->prepare("SELECT `group`,`lastaction`,UNIX_TIMESTAMP(NOW()) FROM `members` WHERE id=? LIMIT 1");
	$stmt->bind_param('i',$id); 
	$stmt->execute();
	$stmt->bind_result($db_group, $db_last_action,$db_now);
	$stmt->fetch();
	$stmt->close();

	if($currentUser->getGroup() == $db_group || !$db_group) {
		$return[1] = "Invalid Request";
		die(json_encode($return));
	}
	
	//Get profile info

	$z = new zipcode_class;
	
	$stmt = $mysqli->prepare("SELECT `id`, `name`, `heightF`, `heightI`, `age`, `zip`, `iAm`, `seeking`, `bodyType`, `ethnicity`, `lookingFor`, `description`, `viewedMe`, `min_offer`, `best_chance` FROM `profile` WHERE id=?");
	$stmt->bind_param('i',$id);
	$stmt->execute();
	$stmt->bind_result($db_id,$db_name,$db_heightF,$db_heightI,$db_age,$db_zip,$db_iAm,$db_seeking,$db_bodyType,$db_ethnicity,$db_lookingFor,$db_description,$db_viewedMe,$db_min_offer,$db_best_chance);
	$stmt->fetch();
	$stmt->close();
	
	$array = array();
	
	$details = $z->get_zip_details($db_zip);
	if ($details === false)
	   $area = '';
	else
	   $area = $details['city'].', '.$details['state_prefix'];
	   
	$array[$id]['name'] = $db_name;
	$array[$id]['age'] = $db_age;
	$array[$id]['zip'] = $db_zip;
	$array[$id]['area'] = $area;
	$array[$id]['heightF'] = $db_heightF;
	$array[$id]['heightI'] = $db_heightI;
	$array[$id]['iAm'] = $db_IAm;
	$array[$id]['seeking'] = $db_seeking;
	$array[$id]['bodyType'] = $db_bodyType;
	$array[$id]['ethnicity'] = $db_ethnicity;
	$array[$id]['lookingFor'] = $db_lookingFor;
	$array[$id]['description'] = $db_description;
	$array[$id]['min_offer'] = $db_min_offer;
	$array[$id]['best_chance'] = $db_best_chance;
	$array[$id]['canMessage'] = $currentUser->checkPermissions($id,0);
	$array[$id]['canViewVideos'] = $currentUser->checkPermissions($id,1);
	
	if(($db_now - $db_last_action) < 15*60)
			$array[$id]['onlineNow'] = 1;
	else
			$array[$id]['onlineNow'] = 0;
	
	$queue = new Queue(10);
	//Set the viewed me
	if(!$db_viewedMe) { //first time
		$queue->enqueue($currentUser->getId());
		$db_viewedMe = json_encode($queue);
	}else{
		$queue->populateFromJSON($db_viewedMe);
		$queue->enqueue($currentUser->getId());
		$db_viewedMe = json_encode($queue);
	}
	
	$stmt = $mysqli->prepare("UPDATE `profile` SET `viewedMe`=? WHERE id=?");
	$stmt->bind_param('si',$db_viewedMe, $id);
	$stmt->execute();

	$return[0]=1;
	$return[1]=$array;
	
	die(json_encode($return));
 
}

?>