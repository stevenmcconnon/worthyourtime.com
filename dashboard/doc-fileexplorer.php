<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>White Label - a full featured Admin Skin</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/script.js"></script>
	
	
</head>
<body>
				<div id="pageoptions">
			<ul>
				<li><a href="login.php">Logout</a></li>
				<li><a href="#" id="wl_config">Configuration</a></li>
				<li><a href="#">Settings</a></li>
			</ul>
			<div>
						<h3>Place for some configs</h3>
						<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores.</p>
			</div>
		</div>

			<header>
		<div id="logo">
			<a href="test.php">Logo Here</a>
		</div>
		<div id="header">
			<ul id="headernav">
				<li><ul>
					<li><a href="icons.php">Icons</a><span>300+</span></li>
					<li><a href="#">Submenu</a><span>4</span>
						<ul>
							<li><a href="#">Just</a></li>
							<li><a href="#">another</a></li>
							<li><a href="#">Dropdown</a></li>
							<li><a href="#">Menu</a></li>
						</ul>
					</li>
					<li><a href="login.php">Login</a></li>
					<li><a href="wizard.php">Wizard</a><span>Bonus</span></li>
					<li><a href="#">Errorpage</a><span>new</span>
						<ul>
							<li><a href="error-403.php">403</a></li>
							<li><a href="error-404.php">404</a></li>
							<li><a href="error-405.php">405</a></li>
							<li><a href="error-500.php">500</a></li>
							<li><a href="error-503.php">503</a></li>
						</ul>
					</li>
				</ul></li>
			</ul>
			<div id="searchbox">
				<form id="searchform" autocomplete="off">
					<input type="search" name="query" id="search" placeholder="Search">
				</form>
			</div>
			<ul id="searchboxresult">
			</ul>
		</div>
	</header>

				<nav>
			<ul id="nav">
				<li class="i_house"><a href="dashboard.php"><span>Dashboard</span></a></li>
				<li class="i_book"><a><span>Documentation</span></a>
					<ul>
						<li><a href="doc-alert.php"><span>Alert Boxes</span></a></li>
						<li><a href="doc-breadcrumb.php"><span>Breadcrumb</span></a></li>
						<li><a href="doc-calendar.php"><span>Calendar</span></a></li>
						<li><a href="doc-charts.php"><span>Charts</span></a></li>
						<li><a href="doc-dialog.php"><span>Dialog</span></a></li>
						<li><a href="doc-editor.php"><span>Editor</span></a></li>
						<li><a href="doc-file.php"><span>File</span></a></li>
						<li><a href="doc-fileexplorer.php"><span>Fileexplorer</span></a></li>
						<li><a href="doc-form.php"><span>Form</span></a></li>
						<li><a href="doc-gallery.php"><span>Gallery</span></a></li>
						<li><a href="doc-inputfields.php"><span>Inputfields</span></a></li>
						<li><a href="doc-slider.php"><span>Slider</span></a></li>
						<li><a href="doc-store.php"><span>Store</span></a></li>
						<li><a href="doc-widget.php"><span>Widget</span></a></li>
					</ul>
				</li>
				<li class="i_create_write"><a href="form.php"><span>Form</span></a></li>
				<li class="i_graph"><a href="charts.php"><span>Charts</span></a></li>
				<li class="i_images"><a href="gallery.php"><span>Gallery</span></a></li>
				<li class="i_blocks_images"><a href="widgets.php"><span>Widgets</span></a></li>
				<li class="i_breadcrumb"><a href="breadcrumb.php"><span>Breadcrumb</span></a></li>
				<li class="i_file_cabinet"><a href="fileexplorer.php"><span>Fileexplorer</span></a></li>
				<li class="i_calendar_day"><a href="calendar.php"><span>Calendar</span></a></li>
				<li class="i_speech_bubbles_2"><a href="dialogs_and_buttons.php"><span>Dialogs &amp; Buttons</span></a></li>
				<li class="i_table"><a href="datatable.php"><span>Table</span></a></li>
				<li class="i_typo"><a href="typo.php"><span>Typo</span></a></li>
				<li class="i_grid"><a href="grid.php"><span>Grid</span></a></li>
			</ul>
		</nav>
		
			
		<section id="content">
		
			<div class="g12">
			
			<h1>Fileexplorer <span title="current version: 1.0">v 1.0</span></h1>
			<p>Generats an Fileexplorer. Uses the <a href="http://elrte.org/elfinder">elFinder plugin</a></p>
			
			
			<h3>Options <span>read more about <a href="doc-inline-data-types.php">inline data attributes</a></span></h3>
			<table class="documentation">
			<thead>
				<tr><th>option</th><th>default</th><th>possible values</th><th>description</th><th>since</th><th>info</th></tr>
			</thead>
			<tbody>
			<tr><td colspan="6">Check out <a href="http://elrte.org/redmine/projects/elfinder/wiki">elFinder plugin wiki</a> for all options</td>
			</tr>
			</tbody>
			</table>
			
			<h3>Methods</h3>
			<table class="documentation">
			<thead>
			<tr><th>method name</th><th>default</th><th>arguments</th><th>description</th><th>since</th><th>info</th></tr>
			</thead>
			<tbody>
			<tr><th>set</th><td></td><td>key, value</td>
			<td>sets the the option 'key' with the value 'value'</td><td>1.0</td>
			</tr>
			<tr><td colspan="6">Check out <a href="http://elrte.org/redmine/projects/elfinder/wiki">elFinder plugin wiki</a> for all methods</td>
			</tr>
			</tbody>
			</table>
			
			<h3>Usage</h3>
<pre>$(selector).wl_Fileexplorer([options]);</pre>

			<h3>Calling Methods</h3>
<pre>$(selector).wl_Gallery('method', [,arg [,arg]]);</pre>	
			
			<h3>Markup</h3>
<pre>
&lt;div class="fileexplorer"&gt;/div&gt;</pre>

			<h3>Example</h3>
			
			<div class="fileexplorer"></div>

			</div>
		</section><!-- end div #content -->
		<footer>Copyright by revaxarts.com 2011</footer>
</body>
</html>