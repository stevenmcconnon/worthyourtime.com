<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('../includes/initilization.php');	

if(!$currentUser->IsLoggedIn())
     header("Location: /");

$currentUser->resetToken();
$info = json_decode( $currentUser->retJSONInfo() );


?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>Edit Profile - WorthYourTime.com</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>


	
<script>
$(document).ready(function() {

	
	
	

	$('#editProfile').wl_Form({ajax: false});
		
	$('#age').wl_Number();	
	$('#zip').wl_Valid();


		
	$.get("<?php echo $domain; ?>backend/viewProfile.php", {id: "<?php echo $currentUser->id; ?>", enum: "1"  }, 
		function(data){
			//console.log($.param(data[1]["<?php echo $currentUser->id; ?>"]));
			
			<?php if(!$_GET['stop']) { ?>
				window.location = "dashboard-profile.php?" + $.param(data[1]["<?php echo $currentUser->id; ?>"]) + "&stop=1";
			<?php } ?>
		}, 
	"json");
	

	

	$('#editProfile').submit(function(){    			
    	$.post("<?php echo $domain; ?>usercp/backend/editProfile.php?do=updateProf", $(this).serialize(), 
	    	function(data){
	    		if(data[0] == 0)
	    			alert("Did not save, please correct your errors: \n" + data[1]);
	    		else 
	    			$.msg('Your profile information was saved! Success!');    	
	    	},"json");
    	
    	return false;


   	});

		
});
</script>
	
</head>
<body>
	<?php include_once("nav.php"); ?>
			
		<section id="content">
			
			<div class="g12">
			<h1>Profile</h1>
				<!-- This is where the form is -->
		<form method="post" id="editProfile" name="editProfile" action="dashboard-profile.php" >
			<fieldset>
				<label>Personal Info</label>
				<section>
					<label for="name">Name </label>
					<div><input type="text" id="name" name="name" required></div>
				</section>
				<section>
					<label for="age">Age</label>
					<div><input type="number" id="age" name="age" style="width: 100px;" required data-min="18" data-max="99"></div>
				</section>
				<section>
					<label for="iam">I am a </label>
					<div>
						<select name="iam" id="iam" required>
							<optgroup label="Genders">
								<option value="1">Man</option>
								<option value="2">Woman</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="seeking">Seeking </label>
					<div>
						<select name="seek" id="seek" required>
							<optgroup label="Genders">
								<option value="1">Men</option>
								<option value="2">Women</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="lookingFor">Looking for </label>
					<div>
						<select name="lookingFor" id="lookingFor" required>
							<optgroup label="Options">
								<option value="1">Casual</option>
								<option value="2">Friends</option>								
								<option value="3">Long Term</option>
							</optgroup>
						</select>
					</div>
				</section>
				<?php if ($currentUser->group == 2) { ?>
				<section>
					<label for="feet">Payment Info</label>
					<div>Min Offer: $<input type="number" id="min_offer" name="min_offer" style="width: 100px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Best Chance: $<input type="number" id="best_chance" name="best_chance" style="width: 100px;"></div>
				</section>
				<?php } ?>
				<section>
					<label for="zip">Zip </label>
					<div><input type="text" id="zip" name="zip" required data-regex="^[0-9 ]+$"></div>
				</section>
			</fieldset>
			<fieldset>
				<label>About Me</label>
				<section>
					<label for="description">Describe Yourself</label>
					<textarea id="description" name="description" data-autogrow="true"></textarea>
				</section>
			</fieldset>
			<fieldset>
				<label>Appearance Info</label>
				<section>
					<label for="feet">Height</label>
					<div>Feet: <input type="number" id="heightF" name="heightF" style="width: 100px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inches: <input type="number" id="heightI" name="heightI" style="width: 100px;"></div>
				</section>
<section>
					<label for="bodyType">Body Type</label>
					<div>
						<select name="bodyType" id="bodyType">
							<optgroup label="Types">
								<option value="1">Slim</option>
								<option value="2">Muscular</option>
								<option value="3">Athletic</option>
								<option value="4">A few extra pound</option>
								<option value="5">Fat</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="eyeColor">Eye Color </label>
					<div>
						<select name="eyeColor" id="eyeColor">
							<optgroup label="Colors">
								<option value="1">Amber</option>
								<option value="2">Blue</option>
								<option value="3">Green</option>
								<option value="4">Brown</option>
								<option value="5">Grey</option>
								<option value="6">Hazel</option>
								<option value="7">Other</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="hairColor">Hair Color </label>
					<div>
						<select name="hairColor" id="hairColor">
							<optgroup label="Colors">
								<option value="1">Brown</option>
								<option value="2">Black</option>
								<option value="3">Blonde</option>
								<option value="4">Auburn</option>
								<option value="5">Red</option>
								<option value="6">Other</option>
							</optgroup>
						</select>
					</div>
				</section>
				</fieldset>
				<fieldset>
				<label>Basic Info</label>
				<section>
					<label for="religion">Religion </label>
					<div>
						<select name="religion" id="religion">
							<optgroup label="Religions">
								<option value="1">African Traditional & Diasporic</option>
								<option value="2">Agnostic</option>
								<option value="3">Atheist</option>
								<option value="4">Bahai</option>
								<option value="5">Buddhism</option>
								<option value="6">Cao Dai</option>
								<option value="7">Chinese traditional religion</option>
								<option value="8">Christianity</option>
								<option value="9">Hinduism</option>
								<option value="10">Islam</option>
								<option value="11">Jainism</option>
								<option value="12">Juche</option>
								<option value="13">Judaism</option>
								<option value="14">Neo-Paganism</option>
								<option value="15">Nonreligious</option>
								<option value="16">Rastafarianism</option>
								<option value="17">Secular</option>
								<option value="18">Shinto</option>
								<option value="19">Sikhism</option>
								<option value="20">Spiritism</option>
								<option value="21">Tenrikyo</option>
								<option value="22">Unitarian-Universalism</option>
								<option value="23">Zoroastrianism</option>
								<option value="24">primal-indigenous</option>
								<option value="25">Other</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="ethnicity">Ethnicity </label>
					<div>
						<select name="ethnicity" id="ethnicity">
							<optgroup label="Ethnicities">
								<option value="1">White</option>
								<option value="2">Black</option>
								<option value="3">Spanish</option>
								<option value="4">Indian</option>
								<option value="5">Arab</option>
								<option value="6">Asian</option>
								<option value="7">Other</option>
							</optgroup>
						</select>
					</div>
				</section>
				
				
				<section>
					<label for="children">Children </label>
					<div>
						<select name="children" id="children">
							<optgroup label="Options">
								<option value="1">Yes</option>
								<option value="2">No</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="education">Education </label>
					<div>
						<select name="education" id="education">
							<optgroup label="Options">
								<option value="1">High School</option>
								<option value="2">Some College</option>
								<option value="3">College Graduate</option>
								<option value="4">Masters</option>
								<option value="5">Doctorate</option>
							</optgroup>
						</select>
				</section>
				<section>
					<label for="salary">Salary </label>
					<div>
						<select name="salary" id="salary"  required>
							<optgroup label="Options">
								<option value="1">Under $30,000</option>
								<option value="2">$30,000 - $49,999</option>
								<option value="3">$50,000 - $79,000</option>
								<option value="4">$80,000 - $99,999</option>
								<option value="5">$100,000 - $150,000</option>
								<option value="6">Over $150,000</option>
							</optgroup>
						</select>
					</div>
				</section>
				<section>
					<label for="smoking">Smoking </label>
					<div>
						<select name="smoking" id="smoking">
							<optgroup label="Options">
								<option value="1">Never</option>
								<option value="2">Socially</option>
								<option value="3">Sometimes</option>
								<option value="4">Frequently</option>
								<option value="5">Not Cigarettes</option>
							</optgroup>
						</select>
				</section>
				<section>
					<label for="drinking">Drinking </label>
					<div>
						<select name="drinking" id="drinking">
							<optgroup label="Options">
								<option value="1">Never</option>
								<option value="2">Socially</option>
								<option value="3">Sometimes</option>
								<option value="4">Frequently</option>
							</optgroup>
						</select>
				</section>

				<section>
					<label for="occupation">Occupation </label>
					<div><input type="text" id="occupation" name="occupation" required></div>
				</section>
				<section>
					<label for="salary">Salary </label>
					<div>
						<select name="salary" id="salary"  required>
							<optgroup label="Options">
								<option value="1">Under $30,000</option>
								<option value="2">$30,000 - $49,999</option>
								<option value="3">$50,000 - $79,000</option>
								<option value="4">$80,000 - $99,999</option>
								<option value="5">$100,000 - $150,000</option>
								<option value="6">Over $150,000</option>
							</optgroup>
						</select>
					</div>
				</section>
				<button name="submit" type="submit" class="submit" style="margin-left: 750px; " id="submit">Save</button>
					</div>
				</section>
				</fieldset>
				
		</form>
				</div>
		</section><!-- end div #content -->
</body>
</html>