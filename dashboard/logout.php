<?php
	session_start();
	session_destroy();
	
	define("_CWD_", getcwd());
	require_once('../includes/initilization.php');	
	
?>
<script>
	window.location = "<?php echo $domain; ?>";
</script>