<?php
define("_CWD_", getcwd());
require_once('../../includes/initilization.php');	


$offerId = $_GET['offerId'];
$id = $_GET['id'];
$minAmt = $_GET['minAmt'];

?>

<script src="<?=$domain;?>dashboard/ajaxContent/js/counterOffer.js"></script>
<script src="<?=$domain;?>dashboard/js/wl_form.js"></script>
<script>
$('#outerArea').wl_Form();
	$(document).ready(function(){
		$("#submit").button();
		
		
		$("#submit").click(function(){
		
			amount = $("#amount").val();
			
			$("#outerArea").html("You counter offer of $" + amount + " has been sent successfully.");
			
			counterOffer("<?php echo $domain; ?>", "<?php echo $_GET['offerId']; ?>", amount, function(result) {
				if(result[0] == 1) {
					$("#outerArea").html("You counter offer of $" + amount + " has been sent successfully.");
				} else {
					$("#outerArea").html("You counter offer could not be made.");
				}
			});
		});

	});
</script>

<div id="outerArea" style="margin: 0 auto; text-align: center;">
	<label for="amount">Amount: </label>
	<select id="amount" name="amount">
	<?php 
		  echo ($minAmt < 10) ? '<option value="10">$10.00</option>' : NULL;
		  echo ($minAmt < 25) ? '<option value="25">$25.00</option>' : NULL;
		  echo ($minAmt < 50) ? '<option value="50">$50.00</option>' : NULL;
		  echo ($minAmt < 75) ? '<option value="75">$75.00</option>' : NULL;
		  echo ($minAmt < 100) ? '<option value="100">$100.00</option>' : NULL;
		  echo ($minAmt < 125) ? '<option value="125">$125.00</option>' : NULL;
		  echo ($minAmt < 150) ? '<option value="150">$150.00</option>' : NULL;
		  echo ($minAmt < 175) ? '<option value="175">$175.00</option>' : NULL;
		  echo ($minAmt < 200) ? '<option value="200">$200.00</option>' : NULL;
		  echo ($minAmt < 225) ? '<option value="225">$225.00</option>' : NULL;
		  echo ($minAmt < 250) ? '<option value="250">$250.00</option>' : NULL;
		  echo ($minAmt < 275) ? '<option value="275">$275.00</option>' : NULL;
		  echo ($minAmt < 300) ? '<option value="300">$300.00</option>' : NULL;
		  echo ($minAmt < 325) ? '<option value="325">$325.00</option>' : NULL;
		  echo ($minAmt < 350) ? '<option value="350">$350.00</option>' : NULL;
		  echo ($minAmt < 375) ? '<option value="375">$375.00</option>' : NULL;
		  echo ($minAmt < 400) ? '<option value="400">$400.00</option>' : NULL;
		  echo ($minAmt < 425) ? '<option value="425">$425.00</option>' : NULL;
		  echo ($minAmt < 450) ? '<option value="450">$450.00</option>' : NULL;
		  echo ($minAmt < 475) ? '<option value="475">$475.00</option>' : NULL;
		  echo ($minAmt < 500) ? '<option value="500">$500.00</option>' : NULL;
		  echo ($minAmt < 550) ? '<option value="550">$550.00</option>' : NULL;
		  echo ($minAmt < 600) ? '<option value="600">$600.00</option>' : NULL;
		  echo ($minAmt < 650) ? '<option value="650">$650.00</option>' : NULL;
		  echo ($minAmt < 700) ? '<option value="700">$700.00</option>' : NULL;
		  echo ($minAmt < 750) ? '<option value="750">$750.00</option>' : NULL;
		  echo ($minAmt < 800) ? '<option value="800">$800.00</option>' : NULL;
		  echo ($minAmt < 850) ? '<option value="850">$850.00</option>' : NULL;
		  echo ($minAmt < 900) ? '<option value="900">$900.00</option>' : NULL;
		  echo ($minAmt < 950) ? '<option value="950">$950.00</option>' : NULL;
		  echo ($minAmt < 1000) ? '<option value="1000">$1,000.00</option>' : NULL;
		  echo ($minAmt < 1050) ? '<option value="1050">$1,050.00</option>' : NULL;
		  echo ($minAmt < 1100) ? '<option value="1100">$1,100.00</option>' : NULL;
		  echo ($minAmt < 1150) ? '<option value="1150">$1,150.00</option>' : NULL;
		  echo ($minAmt < 1200) ? '<option value="1200">$1,200.00</option>' : NULL;
		  echo ($minAmt < 1250) ? '<option value="1250">$1,250.00</option>' : NULL;
		  echo ($minAmt < 1300) ? '<option value="1300">$1,300.00</option>' : NULL;
		  echo ($minAmt < 1350) ? '<option value="1350">$1,350.00</option>' : NULL;
		  echo ($minAmt < 1400) ? '<option value="1400">$1,400.00</option>' : NULL;
		  echo ($minAmt < 1450) ? '<option value="1450">$1,450.00</option>' : NULL;
		  echo ($minAmt < 1500) ? '<option value="1500">$1,500.00</option>' : NULL;
		  echo ($minAmt < 1550) ? '<option value="1550">$1,550.00</option>' : NULL;
		  echo ($minAmt < 1600) ? '<option value="1600">$1,600.00</option>' : NULL;
		  echo ($minAmt < 1650) ? '<option value="1650">$1,650.00</option>' : NULL;
		  echo ($minAmt < 1700) ? '<option value="1700">$1,700.00</option>' : NULL;
		  echo ($minAmt < 1800) ? '<option value="1800">$1,800.00</option>' : NULL;
		  echo ($minAmt < 1900) ? '<option value="1900">$1,900.00</option>' : NULL;
		  echo ($minAmt < 2000) ? '<option value="2000">$2,000.00</option>' : NULL;
		  echo ($minAmt < 2500) ? '<option value="2500">$2,500.00</option>' : NULL;
		  echo ($minAmt < 3000) ? '<option value="3000">$3,000.00</option>' : NULL;
		  echo ($minAmt < 3500) ? '<option value="3500">$3,500.00</option>' : NULL;
		  echo ($minAmt < 4000) ? '<option value="4000">$4,000.00</option>' : NULL;
		  echo ($minAmt < 4500) ? '<option value="4500">$4,500.00</option>' : NULL;
		  echo ($minAmt < 5000) ? '<option value="5000">$5,000.00</option>' : NULL;
		  echo ($minAmt < 5500) ? '<option value="5500">$5,500.00</option>' : NULL;
		  echo ($minAmt < 6000) ? '<option value="6000">$6,000.00</option>' : NULL;
		  echo ($minAmt < 6500) ? '<option value="6500">$6,500.00</option>' : NULL;
		  echo ($minAmt < 7000) ? '<option value="7000">$7,000.00</option>' : NULL;
		  echo ($minAmt < 7500) ? '<option value="7500">$7,500.00</option>' : NULL;
		  echo ($minAmt < 10000) ? '<option value="10000">$10,000.00</option>' : NULL;
		  echo ($minAmt < 20000) ? '<option value="20000">$20,000.00</option>' : NULL;
		  echo ($minAmt < 30000) ? '<option value="30000">$30,000.00</option>' : NULL;
		  echo ($minAmt < 40000) ? '<option value="40000">$40,000.00</option>' : NULL;
		  echo ($minAmt < 50000) ? '<option value="50000">$50,000.00</option>' : NULL;
	  ?>
	</select>
	<br/><br/>
	<input type="submit" id="submit">
	
</div>
