<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('../includes/initilization.php');	

if(!$currentUser->IsLoggedIn())
     header("Location: /");
     
$currentUser->resetToken();
$info = json_decode( $currentUser->retJSONInfo() );
$response[0] = 0; 

?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>View Inbox - WorthYourTime.com</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>	
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	<script src="js/pajinate.js"></script>
	<script src="js/inbox.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
	
	<script src="js/test.js"></script>

<script>
$(document).ready(function() {
	
	
		
	
	//highlight every other thing
	
	
	url = "<?php echo $domain.'usercp/backend/messaging.php?type=enum'; ?>";
	
	getJson(url, function(json) {
		
		//this puts each json object into an array pageItems
		var pageItems = [];
	    $.each(json[1], function(key,value){
	        pageItems.push( { key: key, value: value } );
	    });
	    
	    //sort the array of objects
	    pageItems.sort();
	    
	    
	    //loops through each page item object and adds it li item
	    //after that it uses the paginate plugin to paginate it
		$.each(pageItems, function(index, value) { 
			var mid = value["key"];
  			var subject = value["value"]["subject"];
  			var fromId = value["value"]["from"];
  			var newMessage = value["value"]["new"];
  			var time = value["value"]["time"];
  			var image = "<?php echo $domain; ?>" + "showMedia.php?id=" + fromId + "&num=0&type=photo";
  			
  			//this loads default img if their image has an error
			//this adds the image
			$(window).load(image, function(response, status, xhr) {
				if (xhr["responseText"].length < 60) {
					image = "../images/default.jpg";
				}
				
				if(newMessage) {
	  				$(".inbox_content").append("<li> <div style='padding: 10px;'><img style='vertical-align: middle;' src='" + image + "' height='40px' width='40px'> <span class='pink'> New! </span> - <a href='dashboard-reply.php?mId=" + mid + "'> <b>" + subject + "</b></a></div></li>");
	  			} else {
	  				$(".inbox_content").append("<li> <div style='padding: 10px;'><img style='vertical-align: middle;' src='" + image + "' height='40px' width='40px'><a href='dashboard-reply.php?mId=" + mid + "'> " + subject + "</a></div> </li>");
	  			}
	  			
	  			$('#inbox li:odd, .content > *:even').css('background-color','#e4f5ff');
	  			
	  			$('#inbox').pajinate({
					items_per_page : 5,
					item_container_id : '.inbox_content',
					nav_panel_id : '.alt_page_navigation',
					abort_on_small_lists: true
				});
				
				
			});
  			
  			
  			
		});
		
		
		
	});
	
});

</script>
	
	
</head>
<body>

			<?php include_once("nav.php"); ?>
		<section id="content">
				<h2>Inbox</h2>
				<br/>

<div class="container pretty_container" id="inbox">

				
	<ul class="inbox_content">
	<!-- content can go in here -->
	</ul>
	<div class="alt_page_navigation"></div>
	<div style="clear:both;"></div>
	
</div>
		
<div style="clear:both;"></div>

	  </div>
	  
			<p>&nbsp;</p>
		<footer>Copyright by WorthYourTime.com 2012</footer>
		
</body>
</html>