<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('../includes/initilization.php');	

if(!$currentUser->IsLoggedIn())
     header("Location: /");
     
$currentUser->resetToken();
$info = json_decode( $currentUser->retJSONInfo() );
$response[0] = 0; 

//if the image form was submitted
if($_POST['photoSubmit']) {

	$postArgs = Array();
	
	
	//put each post variable into the array
	foreach ($_POST as $key => $value) {
		$postArgs[$key] = $value;
	}
	
	//put all the file info into the array
	foreach ($_FILES['file'] as $key => $value) {
		$postArgs[$key] = $value;
	}
	
	
	$postArgs['file'] = '@'.$_FILES['file']['tmp_name'];
	
	//upload the image
	//stuff needed to transfer the session
	$strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
	session_write_close();

	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "$domain"."usercp/backend/adminPhoto.php?do=upload");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postArgs);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt( $ch, CURLOPT_COOKIE, $strCookie );

	$response = curl_exec($ch);
	$response = json_decode($response);
	curl_close($ch);
}

?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>Edit Photos - WorthYourTime.com</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>	
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
	
	<script src="js/test.js"></script>

<script>
$(document).ready(function() {
	console.log(<?php echo $response[0]; ?>);
	if (<?php echo $response[0]; ?> == 1) {
		$.wl_Alert('Your photo was successfully uploaded!', 'success', '#content');
	} else if (<?php echo $response[0]; ?> == 2) {
		$.wl_Alert('Error: You have uploaded the maximum amount of photos, your photo was NOT uploaded.', 'warning', '#content');
	}
	
	$('.gallery').wl_Gallery({ fancybox:{'type' : 'image'}, 'editBtn' : false, 
	'onDelete' : function (element, href, title){
					url = getUrlVars(href);
					$.confirm('Are you sure you want to delete this photo?', function(){
						$.get("/usercp/backend/adminPhoto.php?do=remove&type=photo", {"num" : url.num},
							function(data){
								if(data[0] == 1)
									window.location.reload();
								else
									$.wl_Alert('Error: Could not remove photo.', 'warning', '#content');	
							},
						"json");
					});
				
				
				}, 
	'onMove' : function (element, href, title, newOrder){
					console.log(newOrder);
				} 
	});
		

	function getUrlVars(url) {
		var vars = {};
		var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, 
			function(m,key,value) {
				vars[key] = value;
			}
		);
    	return vars;
	}


});

</script>
	
	
</head>
<body>

	<?php include_once("nav.php"); ?>
		<section id="content">
				
<!-- 		This is where the Gallery is -->
		<ul class="gallery">
		<?php
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,"$domain/showMedia.php?id=".$info->id."&type=photo&numMedia=1");
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
			$result = json_decode(curl_exec($ch));
			curl_close($ch);
			
			if($result[1] < 1)
				echo "<h2>You have not uploaded any photos yet.</h2>";
			

			for ($i=0; $i < $result[1]; $i++ ) {
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,"$domain/showMedia.php?id=". $info->id . "&num=$i&type=photo&info=1");
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
				$imgInfo = json_decode(curl_exec($ch));
				curl_close($ch);				
			 	
				echo "<li>
						<a href='$domain"."showMedia.php?id=". $info->id . "&num=$i&type=photo' ".
						"title='". ($i+1) ."'>".
						"<img src='$domain/showMedia.php?id=". $info->id . "&num=$i&type=photo' width='116' height='116'></a>
					</li>";
			} 
			
			

		?>
		</ul>
		
		
<!-- This is where the upload form is -->
		<form method="post" id="photoUploader" enctype="multipart/form-data">
			<fieldset>
				<label>Upload A Photo</label>
				<section>
					<label for="input">Name <span class="required">&nbsp;</span></label>
					<div><input type="text" id="name1" name="name1" required></div>
				</section>
				<section>
					<label for="input">Description</label>
					<div><input type="text" id="description" name="description"></div>
				</section>
				<section>
					<label for="input">Photo <span class="required">&nbsp;</span></label>
					<div><input type="file" id="file" name="file" required></div>
					<div><input type="hidden" id="id" name="id" value="<?php echo $currentUser->id; ?>"></div>
					</section>
				<section>
					<div><button id="photoSubmit" name="photoSubmit" value="photoSubmit">Upload Photo</button></div>
				</section>
			</fieldset>
		</form>
		


	  </div>
	  
			<p>&nbsp;</p>
		<footer>Copyright by WorthYourTime.com 2012</footer>
		
</body>
</html>