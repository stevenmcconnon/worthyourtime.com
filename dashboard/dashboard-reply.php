<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('../includes/initilization.php');	

if(!$currentUser->IsLoggedIn())
     header("Location: /");
     
$currentUser->resetToken();
$info = json_decode( $currentUser->retJSONInfo() );
$response[0] = 0; 

?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>View/Reply to Message - WorthYourTime.com</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>	
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	<script src="js/pajinate.js"></script>
	<script src="js/inbox.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
	
	<script src="js/test.js"></script>

<script>
$(document).ready(function() {
	
	
		
	
	//highlight every other thing
	
	
	url = "<?php echo $domain.'usercp/backend/messaging.php?type=view&mId=' . $_GET['mId']; ?>";
	
	getJson(url, function(json) {
		subject = json[1]["0"]["subject"];
		body = json[1]["0"]["body"];
		fromId = json[1]["0"]["from"];
		newMessage = json[1]["0"]["new"];
		time = json[1]["0"]["time"];
		image = "<?php echo $domain; ?>" + "showMedia.php?id=" + fromId + "&num=0&type=photo"
		
		//this adds the subject
		$(".subject").append(" " + subject);
		$("#replySubject").attr("value", "Re: " + subject);
		
		//this adds the body
		$("#body").append(" " + body);
		
		//this adds the image
		$(window).load(image, function(response, status, xhr) {
			if (xhr["responseText"].length > 50) {				
				$("#image").prepend("<img src='" + image + "' height='70px' width='70px' style='vertical-align:middle;'> ");
		  	} else {
				$("#image").prepend("<img src='../images/default.jpg' height='70px' width='70px' style='vertical-align:middle;'> ");
			}
		});
		
		
		
	});
	
});

</script>
	
	
</head>
<body>
			<?php include_once("nav.php"); ?>
		<section id="content">
			<div>
				<div id="image" style="float:left;">
					<!-- user image goes here -->
				</div>
				
				<div style="float:left; padding: 20px;">
					<h2 class="subject">
					<!-- message subject goes here -->
					</h2>
				</div>
				<div style="clear:both;"></div>
			</div>
				<br/>

<div class="pretty_container" >
	<div id="body">
		<!-- message body goes here -->
	</div>
</div>

<form action="submit.php" method="post">
	<fieldset>
		<label>Reply to this message: </label>
		<section><label for="input">Subject:</label>
			<div><input type="text" id="replySubject" name="input" required></div>
		</section>
		<section><label for="body">Body:</label>
			<div><textarea  type="text" id="replyBody" name="body" required>
				</textarea>
			</div>
		</section>
		<section>
			<div><button>Send Message</button></div>
		</section>
	</fieldset>
</form>
		
<div style="clear:both;"></div>

	  </div>
	  
			<p>&nbsp;</p>
		<footer>Copyright by WorthYourTime.com 2012</footer>
		
</body>
</html>