
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="../css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="../css/grid.css" type="text/css" media="all">
  <link rel="stylesheet" href="../css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="../css/prettyPhoto.css" type="text/css" media="screen"> 
  <script type="text/javascript" src="../js/jquery-1.4.2.min.js" ></script>
<script type="text/javascript" src="../js/contact-form.js"></script>
<!--[if lt IE 7]><div style=' clear: both; height: 59px; text-align:center; position: relative;'> <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" height="42" width="820" alt="" /></a></div><![endif]-->
<!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]-->
<!--[if lt IE 9]><link rel="stylesheet" href="css/ie_style.css" type="text/css" media="screen"><![endif]-->
<script src="../js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/hover-image.js"></script>
<script type="text/javascript" src="../js/superfish.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $('ul.sf-menu').superfish();
});
</script>  
<style type="text/css">
.style1 {color: #FF0000}
</style>
</head>
<body>
<header>
    <div class="main">
        <h1><a href="../index.php">dating</a></h1>
      <div class="indent"><a href="../index-login.php" class="but-1">Login</a><a href="../index-join.php" class="but-1">sign up</a></div>
        <div class="inside">
         <nav>
                <ul class="sf-menu">
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../index-search.php">Search</a>
                    	
                    </li>
                    <li><a href="../index-join.php">Join</a></li>
                    <li><a href="../index-blog.php">Blog</a></li>
                    <li><a href="../index-faq.php">Faqs</a></li>
                    <li></li>
                </ul>
          </nav>
        </div>
    </div>
 
</header>
<aside class="aside2"></aside>
<section id="content">
    <div class="main">
    	<a href="#"><img alt="" src="../images/banner.jpg" class="banner" /></a>
        
<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins -->
	<script src="js/plugins.js"></script>
		
	<!-- Whitelabel Plugins -->
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Form.js"></script>
		
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/login.js"></script>
</head>
<body id="login">
<section id="content">
		<form action="submit.php" id="loginform">
			<fieldset>
				<section><label for="username">Username</label>
					<div><input type="text" id="username" name="username" autofocus></div>
				</section>
				<section><label for="password">Password <a href="#">lost password?</a></label>
					<div><input type="password" id="password" name="password"></div>
					<div><input type="checkbox" id="remember" name="remember"><label for="remember" class="checkbox">remember me</label></div>
				</section>
				<section>
					<div><button class="fr submit">Login</button></div>
				</section>
			</fieldset>
		</form>
		</section>
</body>
              </div>
            </div>            
        </div>
    </div>
</section>
<footer>      
    <div class="main">
        <div class="inside">
            <div class="container">
                <div class="fleft">
                	<ul>
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../index-search.php">Search</a></li>
                        <li><a href="../index-join.php">Join</a></li>
                        <li><a href="../index-blog.php">Blog</a></li>
                        <li><a href="../index-faq.php">FAQs</a></li>
                        <li><a href="../index-contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="fright"><span>Worth Your Time</span> &nbsp;&copy; 2011 &nbsp; &nbsp;<a href="../index-privacy.php">Privacy policy</a> &nbsp;<!--{%FOOTER_LINK}--></div>     
            </div>   
        </div>
    </div>   
</footer>    

<script type="text/javascript">
		$(document).ready(function(){			
			//for prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
</script>
<!-- coded by Ann -->
</body>
</html>
