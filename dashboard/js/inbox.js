
function getJson(url, callback) {
	return $.ajax({
		url: url,
        type: 'get',
        dataType: 'json',
        async: true,
        success: function(data) {
            callback(data);
        } 
	});
}


	