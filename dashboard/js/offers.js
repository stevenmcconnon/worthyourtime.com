function getJson(domain, callback) {
	url = domain + "usercp/backend/offers.php?type=view";
	
	console.log(url);

	$.get(url, function(result) {
		callback(result);
	}, "json");
}


function cancel(domain, offerId, callback) {
	url = domain + "usercp/backend/offers.php?offerId=" + offerId + "&type=rescind";
	
	console.log(url);
	
	$.get(url, function(result) {
		callback(result);
	}, "json");
}

function accept(domain, offerId, callback) {
	url = domain + "usercp/backend/offers.php?offerId=" + offerId + "&type=accept";
	
	console.log(url);
	
	$.get(url, function(result) {
		callback(result);
	}, "json");
}


function reject(domain, offerId, callback) {
	url = domain + "usercp/backend/offers.php?offerId=" + offerId + "&type=reject";
	
	console.log(url);
	
	$.get(url, function(result) {
		callback(result);
	}, "json");
}