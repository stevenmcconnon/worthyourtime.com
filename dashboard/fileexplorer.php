<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>White Label - a full featured Admin Skin</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/script.js"></script>
	
	
</head>
<body>
				<div id="pageoptions">
			<ul>
				<li><a href="login.php">Logout</a></li>
				<li><a href="#" id="wl_config">Configuration</a></li>
				<li><a href="#">Settings</a></li>
			</ul>
			<div>
						<h3>Place for some configs</h3>
						<p>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores.</p>
			</div>
		</div>

			<header>
		<div id="logo">
			<a href="test.php">Logo Here</a>
		</div>
		<div id="header">
		  <ul id="headernav">
		    <li>
		      <ul>
		        <li><a href="icons.php">My Account</a><span>300+</span></li>
		        <li><a href="#">Mailbox</a><span>4</span>
		          <ul>
		            <li><a href="#">Offers</a></li>
		            <li><a href="#">Messages</a></li>
		            <li><a href="#">Flirts</a></li>
		            <li><a href="#">Dates</a></li>
	              </ul>
	            </li>
		        <li><a href="wizard.php">Profile Wizard</a><span>Bonus</span></li>
		        <li><a href="#">Buy Credits</a><span>new</span>
		          <ul>
		            <li><a href="error-403.php">403</a></li>
		            <li><a href="error-404.php">404</a></li>
		            <li><a href="error-405.php">405</a></li>
		            <li><a href="error-500.php">500</a></li>
		            <li><a href="error-503.php">503</a></li>
	              </ul>
	            <li><a href="#">Search</a><span>new</span> </li>
	          </ul>
	        </li>
	      </ul>
		  <div id="searchbox">
		    <form id="searchform" autocomplete="off">
		      <input type="search" name="query" id="search" placeholder="Search">
	        </form>
	      </div>
		  <ul id="searchboxresult">
	      </ul>
		  </div>
		</header>
			<nav>
			  <ul id="nav">
			    <li class="i_house"><a href="dashboard.php"><span>My Dashboard</span></a></li>
			    <li class="i_book"><a><span>My Offers</span></a>
			      <ul>
			        <li><a href="datatable.php"><span>Offers</span></a></li>
			        <li><a href="datatable-dates.php"><span>Dates</span></a></li>
			        <li><a href="datatable-messages.php"><span>Messages</span></a></li>
			        <li><a href="datatable-flirts.php"><span>Flirts</span></a></li>
			        <li><a href="datatable-sent.php"><span>Sent</span></a></li>
			        <li><a href="doc-editor.php"><span>Extra</span></a></li>
		          </ul>
		        </li>
			    <li class="i_create_write"><a href="doc-form.php"><span>My Profile</span></a></li>
			    <li class="i_graph"><a href="charts.php"><span>My Account</span></a></li>
			    <li class="i_images"><a href="gallery.php"><span>My Gallery</span></a></li>
			    <li class="i_blocks_images"><a href="widgets.php"><span>Help</span></a></li>
			    <li class="i_breadcrumb"><a href="breadcrumb.php"><span>Who Viewed Me</span></a></li>
			    <li class="i_file_cabinet"><a href="fileexplorer.php"><span>Favorites</span></a></li>
			    <li class="i_calendar_day"><a href="calendar.php"><span>Calendar</span></a></li>
			    <li class="i_speech_bubbles_2"><a href="dialogs_and_buttons.php"><span>Dialogs &amp; Buttons</span></a></li>
		      </ul>
</nav>
		<section id="content">
		
		
		
			<div class="g12">
			<h1>Fileexplorer <span><a href="doc-fileexplorer.php" class="small">Documentation</a></span></h1>
			<p>A Finder for you online stuff</p>
			<div class="fileexplorer"></div>
			
			</div>

			
			
		</section><!-- end div #content -->
		<footer>Copyright by revaxarts.com 2011</footer>
</body>
</html>