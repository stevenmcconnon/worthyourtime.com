<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('../includes/initilization.php');	

header("Location: $domain" . "dashboard/dashboard-gallery.php"); 


if(!$currentUser->IsLoggedIn()) header("Location: /");

$currentUser->resetToken();
$info = $currentUser->retJSONInfo();

?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>Worth Your Time - Dashboard</title>
	
	<meta name="description" content="">
	<meta name="author" content="Worth Your Time">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/script.js"></script>
	<script src="/frontend_js/library.js"></script>
	<script type="text/javascript">
		var offersInfo;
		var json = <? echo $info; ?>;
		
		function getProfile (id) {
			// profile
			var url = "/backend/viewProfile.php?id=" + id;
//			var profInfo = null;
			return $.post(
				url,
				{    },
				function(responseText){
					console.log("response is " + responseText);
					if (responseText[0] == '0') {
						console.log("Failed to get profile: " + responseText[1]);
					}
					if (responseText[0] == '1') {
						console.log("successfully retrieved profile");
//						profInfo = responseText[1][0];
					}
					$.each(profInfo, function(i,item){
						console.log(" profInfo(id=" + id + ") item (" + i + ") is " + item);
					});
				},
				"json"
			);
		}

		function updateOffersOnPage (responseText) {
					var r = new Array(), j = -1;
					r[++j] ='<table><tr><th>from</th><th>to</th><th>type</th><th>status</th><th>amount</th><th>counter amt</th></tr>';
					$.each(responseText[1], function(i,item){
						r[++j] ='<tr>';
						r[++j] ='<td>' + item.fromUser + '</td>';
						r[++j] ='<td>' + item.toUser + '</td>';
						r[++j] ='<td>' + item.type + '</td>';
						r[++j] ='<td>' + item.status + '</td>';
						r[++j] ='<td>' + item.amount + '</td>';
						r[++j] ='<td>' + item["counter-amount"] + '</td>';
						r[++j] = '</tr>';
					});				
					r[++j] ='</table>';
					$('#tabs-1').html(r.join('')); 
		}

		function updateMsgsOnPage (responseText) {
					var r = new Array(), j = -1;
					r[++j] ='<table><tr><th>from</th><th>subject</th><th>new</th><th>time</th></tr>';
					$.each(responseText[1], function(i,item){
						r[++j] ='<tr>';
						r[++j] ='<td>' + item.from + '</td>';
						r[++j] ='<td>' + item.subject + '</td>';
						r[++j] ='<td>' + item.new + '</td>';
						r[++j] ='<td>' + item.time + '</td>';
						r[++j] = '</tr>';
					});				
					r[++j] ='</table>';
					$('#msgInbox').html(r.join('')); 
		}

		function updateSentMsgsOnPage (responseText) {
					var r = new Array(), j = -1;
					r[++j] ='<table><tr><th>to</th><th>subject</th><th>new</th><th>time</th></tr>';
					$.each(responseText[1], function(i,item){
						r[++j] ='<tr>';
						r[++j] ='<td>' + item.to + '</td>';
						r[++j] ='<td><a href="tid=' + item.tid + '&mid=' + i + '">' + item.subject + '</a></td>';
						r[++j] ='<td>' + item.new + '</td>';
						r[++j] ='<td>' + item.time + '</td>';
						r[++j] = '</tr>';
					});				
					r[++j] ='</table>';
					$('#msgSentBox').html(r.join('')); 
		}

		function getAllOffers () {
			var offersUrl="../usercp/backend/offers.php?id=" + json.id + "&type=view";
	
			// offers
			return $.post(
				offersUrl,
				{  token : json.token, submit : "submit"  },
				function(responseText){
					console.log("response is " + responseText);
					if (responseText[0] == '0') {
						console.log("Failed to get offers: " + responseText[1]);
					}
					if (responseText[0] == '1') {
						console.log("successfully retrieved offers");
					}
					updateOffersOnPage (responseText);
				},
				"json"
			);
		}
	</script>
    
	<script type="text/javascript">
	$(document).ready(function() {

		$('#tabs-3').html("Id: <b>" + json.id + "</b><br/>Group: <b>" + json.group + "</b><br/>UserName:<b>" + 
			json.username +	"</b><br/>Token: <b>" + json.token + "</b>");

		console.log("json is " + json + " json.id is " + json.id);
		console.log("json.username is " + json.username + " json.token is " + json.token);
		if (json.group == "0") window.location ="/";
		
		$('#username').html(json.username);

		// click handler for log out id on logout link must be linkLogout
		setLogout();

		// profile
		var searchUrl = "/backend/search.php?do=basic&id=" + json.id + ">";
		var profInfo;
		$.post(
			searchUrl,
			{    },
			function(responseText){
				console.log("response is " + responseText);
				if (responseText[0] == '0') {
					console.log("Failed to get profile: " + responseText[1]);
				}
				if (responseText[0] == '1') {
					console.log("successfully retrieved profile");
					profInfo = responseText[1][0];
				}
				$.each(profInfo, function(i,item){
					console.log(" profInfo item (" + i + ") is " + item);
				});
			},
			"json"
		);

//	[1,{"2":{"name":"Jamie Foxx","age":0,"zip":0,"area":"","heightF":5,"heightI":3,"iAm":null,"seeking":0,"bodyType":"Fat","ethnicity":"Spanish","lookingFor":0,"description":"","min_offer":50,"best_chance":600,"canMessage":true,"canViewVideos":true,"onlineNow":0}}]
		
//		console.log("attempt to get profile 2");
//		var prof2 = getProfile(2);
//		console.log("attempt to my profile 2");
//		getProfile(json.id);


		// received messages
		var searchUrl = "/usercp/backend/messaging.php?type=enum&id=" + json.id ;
		var msgInfo;
		$.post(
			searchUrl,
			{ token : json.token   },
			function(responseText){
				console.log("response is " + responseText);
				if (responseText[0] == '0') {
					console.log("Failed to get messages: " + responseText[1]);
				}
				if (responseText[0] == '1') {
					console.log("successfully retrieved messages");
					msgInfo = responseText[1][0];
				}
				updateMsgsOnPage (responseText);
				$.each(responseText[1], function(i,item){
					console.log(" msgInfo item (" + i + ") is " + item);
					$.each(item, function(x,subitem){
						console.log("   subitem msgInfo item (" + x + ") is " + subitem);
					});
				});
			},
			"json"
		);

		// sent messages
		$.post(
			"/usercp/backend/messaging.php?type=enum&mType=Sent&id=" + json.id,
			{ token : json.token   },
			function(responseText){
				console.log("response is " + responseText);
				if (responseText[0] == '0') {
					console.log("Failed to get messages: " + responseText[1]);
				}
				if (responseText[0] == '1') {
					console.log("successfully retrieved sent messages");
					msgInfo = responseText[1][0];
				}
				updateSentMsgsOnPage (responseText);
			},
			"json"
		);
		getAllOffers();

		// viewed me
		$.post(
			"/backend/search.php?do=viewedMe&id=" + json.id,
			{    },
			function(responseText){
				console.log("getting viewedme response is " + responseText);
				if (responseText[0] == '0') {
					console.log("Failed to get viewedme: " + responseText[1]);
					if (responseText[0] == '1') {
						console.log("successfully retrieved viewedme");
						viewedMeInfo = responseText[1][0];
					}
					$.each(viewedMeInfo, function(i,item){
						$('whoviewedme').append("<br/><b>" + item + "</b>");
						console.log(" viewedMeInfo item (" + i + ") is " + item);
					});
				}
			},
			"json"
		);





	});
	</script>  

	
</head>
<body>
				<div id="pageoptions">
			<ul>
				<li >Welcome <span id="username"></span> <a id="linkLogout" href="#">Logout</a></li>
				<li><a href="#" id="wl_config">Configuration</a></li>
				<li><a href="#">Settings</a></li>
			</ul>
		</div>

			<header>
		<div id="logo">
			<a href="test.php">Logo Here</a>
		</div>
		<div id="header">
			<ul id="headernav">
				<li><ul>
					<li><a href="icons.php">My Account</a><span>300+</span></li>
					<li><a href="datatable.php">Mailbox</a><span>4</span>
						<ul>
							<li><a href="#">Offers</a></li>
							<li><a href="#">Messages</a></li>
							<li><a href="#">Flirts</a></li>
							<li><a href="#">Dates</a></li>
						</ul>
					</li>
					
					<li><a href="wizard.php">Profile Wizard</a><span>Bonus</span></li>
					<li><a href="#">Buy Credits</a><span>new</span>
                    
						<ul>
							<li><a href="error-403.php">403</a></li>
							<li><a href="error-404.php">404</a></li>
							<li><a href="error-405.php">405</a></li>
							<li><a href="error-500.php">500</a></li>
							<li><a href="error-503.php">503</a></li>
						</ul>
                        <li><a href="../index-search.php">Search</a><span>new</span>
					</li>
				</ul></li>
			</ul>
			<div id="searchbox">
				<form id="searchform" autocomplete="off">
					<input type="search" name="query" id="search" placeholder="Search">
				</form>
			</div>
			<ul id="searchboxresult">
			</ul>
		</div>
	</header>
			<nav>
			  <ul id="nav">
			    <li class="i_house"><a href="dashboard.php"><span>My Dashboard</span></a></li>
			    <li class="i_book"><a><span>My Offers</span></a>
			      <ul>
			        <li><a href="datatable.php"><span>Offers</span></a></li>
			        <li><a href="datatable-dates.php"><span>Dates</span></a></li>
			        <li><a href="datatable-messages.php"><span>Messages</span></a></li>
			        <li><a href="datatable-flirts.php"><span>Flirts</span></a></li>
			        <li><a href="datatable-sent.php"><span>Sent</span></a></li>
			        <li><a href="doc-editor.php"><span>Extra</span></a></li>
		          </ul>
		        </li>
			    <li class="i_create_write"><a href="dashboard-profile.php"><span>My Profile</span></a></li>
			    <li class="i_graph"><a href="charts.php"><span>My Account</span></a></li>
			    <li class="i_images"><a href="dashboard-gallery.php"><span>My Gallery</span></a></li>
			    <li class="i_blocks_images"><a href="widgets.php"><span>Help</span></a></li>
			    <li class="i_breadcrumb"><a href="breadcrumb.php"><span>Who Viewed Me</span></a></li>
<!--	 			<li class="i_file_cabinet"><a href="fileexplorer.php"><span>Favorites</span></a></li>
-->			    <li class="i_calendar_day"><a href="calendar.php"><span>Calendar</span></a></li>
			    <li class="i_speech_bubbles_2"><a href="dialogs_and_buttons.php"><span>Dialogs &amp; Buttons</span></a></li>
		      </ul>
</nav>
	<section id="content">
		
		<div class="g12 nodrop">
			<h1>Dashboard</h1>
			<p>This is a quick overview of some features</p>
		</div>	

		
		<div class="g6 widgets">
		
			
			<div class="widget" id="widget_tabs" data-icon="money">
				<h3 class="handle">Offers</h3>
				<div class="tab">
					<ul id="offersUL">
						<li><a href="#tabs-1">New Offers</a></li>
						<li><a href="#tabs-2">Pending Offers</a></li>
						<li><a href="#tabs-3">Flirts</a></li>
					</ul>
					<div id="tabs-1">
						<p>
						</p>
					</div>
					<div id="tabs-2">
						<p>
							Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
						</p>
					</div>
					<div id="tabs-3">
						<p>
							Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.
						</p>
						<p>
							Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.
						</p>
					</div>
				</div>
			</div>
		
			<div class="widget number-widget" id="widget_number" data-icon="filmcamera">
				<h3 class="handle">Who Viewed Me</h3>
				<div id="whoviewedme">
					<ul>
						<li><a href=""><span>43</span> Total Visits</a></li>
						<li><a href=""><span>12</span> Favorited Me</a></li>
						<li><a href=""><span>7</span> Flirted With Me</a></li>
						<li><a href=""><span>0</span> Blah Blah Me</a></li>
						<li><a href=""><span>0</span> Comments</a></li>
					</ul>
				</div>
			</div>
		</div>
		
        
        
        
		
		<div class="g6 widgets">
			<div class="widget" id="widget_charts" data-icon="users">
				<h3 class="handle">Featured Members</h3>
				<div>
					<table class="chart" data-fill="true" data-tooltip-pattern="%1 %2 on the 2011-03-%3">
						<thead>
							<tr>
								<th></th>
								<th>01</th><th>02</th><th>03</th><th>04</th><th>05</th><th>06</th><th>07</th><th>08</th><th>09</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th>
								<th>16</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>Total Visitors</th>
								<td>331</td><td>306</td><td>337</td><td>340</td><td>332</td><td>330</td><td>307</td><td>343</td><td>307</td><td>322</td><td>319</td><td>314</td><td>326</td><td>317</td><td>323</td><td>347</td><td>317</td><td>341</td><td>328</td><td>307</td><td>350</td><td>330</td><td>313</td><td>314</td><td>332</td><td>330</td><td>317</td><td>346</td><td>328</td><td>312</td><td>311</td>
							</tr>
							<tr>
								<th>Unique Visitors</th>
								<td>113</td><td>126</td><td>143</td><td>126</td><td>147</td><td>131</td><td>150</td><td>112</td><td>110</td><td>130</td><td>109</td><td>115</td><td>146</td><td>129</td><td>138</td><td>122</td><td>114</td><td>112</td><td>128</td><td>111</td><td>122</td><td>136</td><td>109</td><td>106</td><td>104</td><td>146</td><td>123</td><td>139</td><td>117</td><td>116</td><td>143</td>
							</tr>
						</tbody>
					</table>
					<p>
					<a class="btn" href="charts.php">Check out the Charts section</a>
					</p>
					</div>
				</div>
				
				<div class="widget" id="widget_accordion" data-icon="mail">
					<h3 class="handle">Mailbox</h3>
					<div class="accordion">
						<h4><a href="#">Inbox</a></h4>
						<div id="msgInbox">
							<p>
								 Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
							</p>
						</div>
						<h4><a href="#">Sent</a></h4>
						<div id="msgSentBox">
							<p>
								 Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
							</p>
						</div>
						<h4><a href="#">Deleted</a></h4>
						<div>
							<p>
								 Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
							</p>
							<ul>
								<li>List item one</li>
								<li>List item two</li>
								<li>List item three</li>
							</ul>
						</div>
						
						
					</div>
				</div>
				
				<div class="widget" id="widget_breadcrumb" data-icon="money_2">
					<h3 class="handle">Buy Credits</h3>
					<div>
						<ul class="breadcrumb" data-numbers="true">
							<li><a href="#">100 Credits</a></li>
							<li><a href="#">500 Credits</a></li>
							<li><a href="#">1000 Credits</a></li>
						</ul>
						<p>
						<a class="btn" href="breadcrumb.php">Buy Now</a>
						</p>
					</div>
				</div>
				
				<div class="widget" id="widget_ajax" data-load="widget-content.php" data-reload="10" data-remove-content="false" data-icon="images">
					<h3 class="handle">My Media Gallery</h3>
					<div>
						This content get replaced
					</div>
				</div>
				
				<div class="widget" id="widget_info" data-icon="question">
					<h3 class="handle">Advice from the Staff</h3>
					<div>
						<h3>Advice from the Staff</h3>
						<p>
						 Widgets are flexible, dragg-, sort-, and collapseable content boxes. Fill them with some html!
						</p>
						<p>
						<a class="btn" href="widgets.php">Check out the FAQ section</a>
						</p>
					</div>
				</div>
				
		  </div>
			
			</section>
		<footer>Worth Your Time 2011</footer>
</body>
</html>