<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('../includes/initilization.php');	

if(!$currentUser->IsLoggedIn())
     header("Location: /");
     
$currentUser->resetToken();
$info = json_decode( $currentUser->retJSONInfo() );
$response[0] = 0; 

?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>View Offers - WorthYourTime.com</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins and Whitelabel Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_File.js"></script>	
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Multiselect.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	<script src="js/pajinate.js"></script>
	<script src="js/offers.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
	
	<script src="js/test.js"></script>

<script>
$(document).ready(function() {
	
	
		
	
	//highlight every other thing
	
	
	var domain = "<?php echo $domain; ?>";
	
	getJson(domain, function(json) {
		//this puts each json object into an array pageItems
		var pageItems = [];
		
	    $.each(json[1], function(key, value){
	        pageItems.push( { key: key, value: value } );
	    });
	    
	    //sort the array of objects
	    pageItems.sort();
	    
	    
	    //loops through each page item object and adds it li item
	    //after that it uses the paginate plugin to paginate it
		$.each(pageItems, function(index, value) {
			var offerId = value["key"];
  			var toId = value["value"]["to"];
  			var toUser = value["value"]["toUser"];
  			var offerStatus = value["value"]["status"];
  			var type = value["value"]["type"];
  			var amount = value["value"]["amount"];
  			var counter = value["value"]["counter-amount"];
  			var image = "<?php echo $domain; ?>" + "showMedia.php?id=" + toId + "&num=0&type=photo";
  			
  			//this loads default img if their image has an error
			//this adds the image
			$(window).load(image, function(response, status, xhr) {
				if (xhr["responseText"].length < 60) {
					image = "../images/default.jpg";
				}
				
				if (offerStatus == "pending" && counter == 0 && "<?= $currentUser->group; ?>" == "2") {
	  				$(".inbox_content").append("<li> <div style='padding: 10px;'><img style='vertical-align: middle;' src='" + image + "' height='40px' width='40px'> " + toUser + "&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount: " + amount + "</b>&nbsp;&nbsp;&nbsp; <b>Pending</b> <button class='small counter' style='float:right;' toid='" + toId + "' offerid='" + offerId + "' minamt='" + amount + "'>Make Counter Offer</button> <button class='small accept' style='float:right;' toid='" + toId + "' offerid='" + offerId + "'>Accept Offer</button> <button class='small reject' style='float:right;' toid='" + toId + "' offerid='" + offerId + "'>Reject Offer</button></div></li>");
	  			} else if (offerStatus == "pending" && counter == 0 && "<?= $currentUser->group; ?>" == "1") {
	  				$(".inbox_content").append("<li> <div style='padding: 10px;'><img style='vertical-align: middle;' src='" + image + "' height='40px' width='40px'> " + toUser + "&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount: " + amount + "</b>&nbsp;&nbsp;&nbsp; <b>Pending</b><button class='small cancel' style='float:right;' toid='" + toId + "' offerid='" + offerId + "'>Cancel Offer</button> </div></li>");
	  			} else if (offerStatus == "unlocked") {
	  				$(".inbox_content").append("<li> <div style='padding: 10px;'><img style='vertical-align: middle;' src='" + image + "' height='40px' width='40px'> " + toUser + "&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount: " + amount + "</b>&nbsp;&nbsp;&nbsp;<b>Unlocked</b> <button class='small messageNow' style='float:right;' toid='" + toId + "' offerid='" + offerId + "'>Message Now!</button> </div></li>");
	  			} else if (offerStatus == "pending" && counter > 0) {
	  				$(".inbox_content").append("<li> <div style='padding: 10px;'><img style='vertical-align: middle;' src='" + image + "' height='40px' width='40px'> " + toUser + "&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount: " + amount + "</b>&nbsp;&nbsp;&nbsp; <b>Counter-Offer: " + counter + "</b><button class='small cancel' style='float:right;'  toId='" + toId + "' offerId='" + offerId + "'>Cancel Offer</button><button class='small accept' style='float:right;' toid='" + toId + "' offerid='" + offerId + "'>Accept Counter Offer</button><button class='small reject' style='float:right;' toid='" + toId + "' offerid='" + offerId + "'>Reject Counter Offer</button> </div></li>");
	  			}
	  			
	  			
	  			$('#inbox li:odd, .content > *:even').css('background-color','#e4f5ff');
	  			
	  			$('#inbox').pajinate({
					items_per_page : 5,
					item_container_id : '.inbox_content',
					nav_panel_id : '.alt_page_navigation',
					abort_on_small_lists: true
				});
				
				
				
							
			});
  			
  			
  			
		});
		
		//default behavior if there are no offers
		if(pageItems.length <= 0)
			$(".inbox_content").append("<li> <div style='padding: 10px;'>You have no offers at this time. </div></li>");
		
	});
	
	
	//code to run when cancel is clicked
	$("body").on("click", ".cancel", function(){
    	offerId = $(this).attr("offerid");
    	block = $(this).parent();
    	
		cancel(domain, offerId, function(result){
			if(result[0] == 1) {
				$(block).remove();
				$.msg('Your offer to this user was cancelled.');
			} else {
				$.msg('Could not cancel offer.');
			}
		});
      
    });
    
    
    //code to run when accept is clicked
	$("body").on("click", ".accept", function(){
    	offerId = $(this).attr("offerid");
					
		accept(domain, offerId, function(result){
			if(result[0] == 1) {
				$.msg('You accepted the counter offer');
			} else {
				$.msg('Could not cancel offer.');
			}
		});
      
    });
    
    
    //code to run when reject is clicked
	$("body").on("click", ".reject", function(){
    	offerId = $(this).attr("offerid");
					
		reject(domain, offerId, function(result){
			if(result[0] == 1) {
				$.msg('You rejected the counter offer');
			} else {
				$.msg('Could not reject offer.');
			}
		});
    });
    
    
    //code to run when counter is clicked
	$("body").on("click", ".counter", function(){
    	offerId = $(this).attr("offerid");
    	minAmt = $(this).attr("minamt");
    	block = $(this).parent();
    	buttons = $(this).parent().find(":button");

					
		$( "#alertBox" ).attr("title", "Make Counter Offer").dialog({
			height: 185,
			width: 600,
			modal: true,
			open: function() {
				$("#alertBox").load("ajaxContent/counterOffer.php?id=<?php echo $_GET['id']; ?>&offerId=" + offerId + "&minAmt=" + minAmt);
			},
			close: function () {
				if($(":contains('successfully.')", this).length > 0) {
				
					//removes the buttons
					$(buttons).each(function() {
						$(this).remove();
					});
					
					//adds a div
					$(block).append("<div style='float:right;'>Counter Offer Sent</div>");
				}
					
			}
		});      
    });
    
    
   	
	
});

</script>
	
	
</head>
<body>

<div id="alertBox" title="" style="display: none;">
	<!-- 	Stuff can go in here, don't forget to add title attr -->
</div>

			<?php include_once("nav.php"); ?>
		<section id="content">
				<h2>Offers</h2>
				<br/>

<div class="container pretty_container" id="inbox">

				
	<ul class="inbox_content">
	<!-- content can go in here -->
	</ul>
	<div class="alt_page_navigation"></div>
	<div style="clear:both;"></div>
	
</div>
		
<div style="clear:both;"></div>

	  </div>
	  
			<p>&nbsp;</p>
		<footer>Copyright by WorthYourTime.com 2012</footer>
		
</body>
</html>