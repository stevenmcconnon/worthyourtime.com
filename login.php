<?
//Gain access to global variables and classes.  Start MySQLi and SESSION
define("_CWD_", getcwd());
require_once('includes/initilization.php');	
$isLoggedIn = $currentUser->IsLoggedIn();

if($isLoggedIn != 1)
	$isLoggedIn = 0;
	
$currentUser->resetToken();
$info = $currentUser->retJSONInfo();



?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/grid.css" type="text/css" media="all">
  <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"> 
  <script type="text/javascript" src="js/jquery-1.4.2.min.js" ></script>
<script type="text/javascript" src="js/contact-form.js"></script>
<!--[if lt IE 7]><div style=' clear: both; height: 59px; text-align:center; position: relative;'> <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" height="42" width="820" alt="" /></a></div><![endif]-->
<!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]-->
<!--[if lt IE 9]><link rel="stylesheet" href="css/ie_style.css" type="text/css" media="screen"><![endif]-->
<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script type="text/javascript" src="js/hover-image.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/login.js"></script>  
<script type="text/javascript">
$(document).ready(function() {
   $('ul.sf-menu').superfish();

	$('#form1').submit(function() {
		processLogin ($("#userName").val(), $("#pw").val(), "<?php echo $domain; ?>", function(result){
			if(result[0] == 1)
				window.location = "dashboard/dashboard.php";
		});
		
		return false;
	});
	
	$('#btnLogin').click(function() {
		$('#form1').submit();
	});
});
</script>
<style type="text/css">
.style1 {color: #FF0000}
</style>
</head>
<body>
<?php include_once("header.php"); ?>

<aside class="aside2"></aside>
<p><img src="images/SilverBanner.png" width="1242" height="62"></p>
<section id="content">
    <div class="main">
      <div class="inside">
          <div class="container_24">
           	  <div class="suffix_1">
           	    <div style="padding: 100px 0 0 250px; background-color: #FFF; background-image: url(images/bg_aside.jpg);">
           	      <div id="login-box">
           	        <h2><strong>Login</strong><br />
       	            </h2>
           	        <div id="login-box-name" style="margin-top:20px; color: #518EE2;">Username:</div>
           	      		<form id = "form1" method="post">
           	        <div id="login-box-field" style="margin-top:10px;">
                    	  <input id="userName" name="q" class="form-login" 
                      			title="Username" value="" size="30" maxlength="2048" />
       	            </div>
           	        <div id="login-box-name" style="margin-top:10px;">Password:</div>
           	        <div id="login-box-field" >
           	          <input id="pw" name="q2" type="password" class="form-login" 
                      			title="Password" value="" size="30" maxlength="2048" />
       	            </div>
           	        <p>&nbsp;</p>
           	        <p>&nbsp;</p>
           	        <p><span class="login-box-options">
       	            <input type="checkbox" name="1"  value="1" 
                    		style="margin-left:-1px; margin-top:5px;">
                    &nbsp; Remember Me &nbsp;    &nbsp;  &nbsp; &nbsp; 
                    <a href="mailto:support@worthyourtime.com"> Forgot password?</a>
                    <a href="#" style="margin-left:55px;"></a></span>
                    <br />
                    <input style="margin-left: -2000px" type="submit" />
                    </form>
           	        <br />
       	            <a href="#"><img src="images/login-btn.png" id="btnLogin" alt="" width="50" height="42" style="margin-left:-1px; width: 100px;" /></a></p>
<p>&nbsp;</p>
           	        <p>&nbsp;</p>
           	        <p>&nbsp;</p>
           	        <p>&nbsp;</p>
       	          </div>
       	        </div>
           	    <p>&nbsp;</p>
              </div>
            </div>            
        </div>
    </div>
</section>
<footer>      
    <div class="main">
        <div class="inside">
            <div class="container">
                <div class="fleft">
                	<ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="index-search.html">Search</a></li>
                        <li><a href="index-join.php">Join</a></li>
                        <li><a href="index-blog.html">Blog</a></li>
                        <li><a href="index-faq.html">FAQs</a></li>
                        <li><a href="index-contact.html">Contact</a></li>
                    </ul>
                </div>
                <div class="fright"><span>Worth Your Time</span> &nbsp;&copy; 2011 &nbsp; &nbsp;<a href="index-privacy.html">Privacy policy</a> &nbsp;<!--{%FOOTER_LINK}--></div>     
            </div>   
        </div>
    </div>   
</footer>    

<script type="text/javascript">
		$(document).ready(function(){			
			//for prettyPhoto
			$("a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
</script>
<!-- coded by Ann -->
</body>
</html>
