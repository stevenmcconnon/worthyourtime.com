<?
	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	require_once('../includes/initilization.php');
	require_once('../includes/classes/recaptchalib.php');
	
	$privatekey = "6LfiL80SAAAAAEcBn5VkrpVbl5TxewggyftHDZ7k";
	$startingCredits = 1000;
	
	//Form submitted POST vars
	$frmUsername = htmlentities($_POST['username'],ENT_QUOTES);
	$frmPassword = $_POST['password'];
	$frmPasswordVerify = $_POST['passwordVerify'];
	$frmAge = intval($_POST['age']);
	$frmEmail = htmlentities($_POST['email'],ENT_QUOTES);
	$frmIAM = intval($_POST['iam']);
	$frmSeek = intval($_POST['seek']);
	$frmZip = htmlentities($_POST['zip'],ENT_QUOTES);
	$group = intval($_POST['group']);
	$submit = $_POST['submit'];
	$frmMinOffer = intval($_POST['minOffer']);
	
	$flags = array();
	$i=0;
	
	$return = array();
	$return[0] = 0;
	//If we are already logged in redirect to the usercp.
	if($currentUser->isLoggedIn()) {
		$return[1] = "Currently logged in";
		die(json_encode($return));
	}
	
	//Validate form
	$errors = array();
	if(isset($submit)) {
		
	    $resp = recaptcha_check_answer ($privatekey,
									$_SERVER["REMOTE_ADDR"],
									$_POST["recaptcha_challenge_field"],
									$_POST["recaptcha_response_field"]);

	    if (!$resp->is_valid)
			$errors[] = 'captcha';
			
		//Check if username is blank
		if (!$frmUsername || strlen($frmUsername) > 15) {
			$errors[] = 'username';
		}
		
		//Do password and verify match
		if (!$frmPassword || ($frmPasswordVerify != $frmPassword)) {
			$errors[] = 'password';		
		}

		//Is email valid
		if (!preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $frmEmail) || strlen($frmEmail) > 255) {
			$errors[] = 'email';
		}
			
		//Between 18 and 99 only
		if (!$frmAge || $frmAge < 18 || $frmAge > 99) {
			$errors[] = 'age';
		}
		
		//Man or woman only
		if (($frmIAM != '1') && ($frmIAM != '2')) {
			$errors[] = 'iam';	
		}
		
		//Seeking a man or woman only
		if (($frmSeek != '1') && ($frmSeek != '2')) {
			$errors[] = 'seek';
		}
		
		//Check ZIP
		if (!$frmZip || strlen($frmZip) != 5) {
			$errors[] = 'zip';
		}
		
		//Generous member or attractive member only
		if (($group != '1') && ($group != '2')) {
			$errors[] = 'group';
		}
		
		//Generous member or attractive member only
		if (($group != '1') && ($group != '2')) {
			$errors[] = 'group';
		}
		
		if (($group == '2') && ($frmMinOffer < 1)) {
			$errors[] = 'minOffer';
		}
		
		 //Grab the user credentials from the DB
		 $stmt = $mysqli->prepare("SELECT COUNT(*) FROM members WHERE username=? OR email=? LIMIT 1");
		 $stmt->bind_param('ss',$frmUsername, $frmEmail);
		 $stmt->execute();
		 $stmt->bind_result($db_count);
		 $stmt->fetch();
		 $stmt->close();
		 
		 //If the username or email exists
		 if($db_count > 0) {
			$errors[] = 'usernameEmailExists';
		 }

		 //Grab the zip credentials from the DB
		 $stmt = $mysqli->prepare("SELECT COUNT(*) FROM zip_code WHERE zip_code=? LIMIT 1");
		 $stmt->bind_param('s',$frmZip);
		 $stmt->execute();
		 $stmt->bind_result($zip_count);
		 $stmt->fetch();
		 $stmt->close();
		 
		 //If the zip is invalid
		 if(intval($zip_count) == 0) {
			$errors[] = 'zip';	 
		 }
		 
		 //If any of the checks failed
		 if(sizeof($errors) > 0) {
			$return[1] = $errors;
			$return[2] = $_POST;
			die(json_encode($return));
		 }
	 
		//Nothing has failed, let's insert
		$ip = $_SERVER['REMOTE_ADDR'];
		$salt = mt_rand(0,getrandmax());
		$joindate = date("Y-m-d");
		$password = md5($salt.$frmPassword);
	 
		$stmt = $mysqli->prepare("INSERT INTO `members` (`username`,`password`,`salt`,`email`,`group`,`iam`,`joindate`,`signup_ip`) VALUES (?,?,?,?,?,?,?,?)");
		$stmt->bind_param('sssssiss',$frmUsername, $password, $salt, $frmEmail, $group, $frmIAM, $joindate, $ip);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
	
		$stmt = $mysqli->prepare("SELECT `id` FROM `members` WHERE username = ? && password = ?");
		$stmt->bind_param('ss',$frmUsername, $password);
		$stmt->execute();
		$stmt->bind_result($id);
		$stmt->fetch();
		$stmt->close();	
			 
		$stmt = $mysqli->prepare("INSERT INTO `profile` (`id`,`age`,`zip`,`iAm`,`seeking`, `min_offer`) VALUES (?,?,?,?,?,?)");
		$stmt->bind_param('iiiiii',$id, $frmAge, $frmZip, $frmIAM, $frmSeek, $frmMinOffer);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		$stmt = $mysqli->prepare("INSERT INTO `user_settings` (`id`) VALUES (?)");

		$stmt->bind_param('i',$id);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();
		
		if($group == '1') {
			$stmt = $mysqli->prepare("INSERT INTO `credits` (`id`,`balance`) VALUES (?,?)");
			$stmt->bind_param('ii',$id, $startingCredits);
			$stmt->execute();
			$stmt->fetch();
			$stmt->close();
		}
		
		EmailWelcome('new_welcome',$frmUsername,$frmEmail);
		
		$return[0] = 1;
		$return[1] = "Success";
		
		die(json_encode($return));
	 //No submit
	 }else{
		$return[1] = "Invalid State";
		die(json_encode($return));
	 }

	function EmailWelcome($predefined, $frmUsername, $to) {
		
		$html = json_decode(file_get_contents(_PREDEFINEDS_.'email/'.$predefined.'.htm'),true);
		$plainText = json_decode(file_get_contents(_PREDEFINEDS_.'email/'.$predefined.'.txt'),true);
		
		$patterns = array();
		$replace = array();
		
		$replace[] = $frmUsername;
		$patterns[] = "/\{toUsername\}/";
		

		foreach ($plainText as $key => $value) {
			$html[$key] = preg_replace($patterns, $replace ,$html[$key]);
			$plainText[$key] = preg_replace($patterns, $replace ,$plainText[$key]);
			
		}
		
		
		require_once('../includes/classes/class.phpmailer.php');

		$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
		$mail->IsSMTP();
		$mail->Host = "localhost";
		$mail->AddReplyTo('no-reply@worthyourtime.com', 'Worthyourtime.com');
		$mail->SetFrom('no-reply@worthyourtime.com', 'Worthyourtime.com');
		$mail->AddAddress($to);
		$mail->Subject = $plainText['subject'];
		$mail->AltBody = $plainText['body']; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML($html['body']);
		$mail->Send();

	}

	
?>
