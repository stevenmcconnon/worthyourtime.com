<?

	//Gain access to global variables and classes.  Start MySQLi and SESSION
	define("_CWD_", getcwd());
	define("_LEGACY_",1);
	require_once('../includes/initilization.php');
	require_once('../includes/classes/zipcode.class.php');
	require_once('../includes/classes/queue.class.php');
	
	//Max fiels per user, hardcoded for now but can be dbed later.
		
	$rate = .15;
	
	//Form submitted POST vars
	$m = $_GET['m'];
	$do = $_GET['do'];
	
	$flags = array();
	$i=0;
	
	$return = array();
	$return[0] = '0';
	
	if($do == "getFeatured") {
		getFeatured();
	}
	
	//If we are not logged in redirect to the login page
	if(!$currentUser->isLoggedIn()) {
		$return[1] = "Permission Denied";
		die(json_encode($return));
	}
	
	switch($do) {
		case "search":
		searchProfile($currentUser);
		break;
		
		case "viewedMe":
		getViewedMe($currentUser);
		break;
		
		case "basic":
		returnBasicInfo($currentUser,$_GET['id']);
		
		default:
		$return[1] = "Invalid Request";
		die(json_encode($return));
		break;
	}

function returnBasicInfo($currentUser,$id) {

	global $mysqli;
	
	$return = array();
	$return[0]=0;
	
	$z = new zipcode_class;
	$whereClause = '';
	
	if($currentUser->getGroup() == '1')
		$stmt = $mysqli->prepare("SELECT t.id, t.name, t.age, t.zip, t.min_offer, t.best_chance, m.lastaction, UNIX_TIMESTAMP(NOW()) FROM profile t JOIN ( SELECT `id`,`group`,`lastaction` FROM members ) m ON t.id = m.id WHERE m.group=2 && t.id=? LIMIT 1");
	else
		$stmt = $mysqli->prepare("SELECT t.id, t.name, t.age, t.zip, t.min_offer, t.best_chance, m.lastaction, UNIX_TIMESTAMP(NOW()) FROM profile t JOIN ( SELECT `id`,`group`,`lastaction` FROM members ) m ON t.id = m.id WHERE m.group=1 && t.id=? LIMIT 1");

	$stmt->bind_param('i',$id);
	$stmt->execute();
	$stmt->bind_result($db_id,$db_name,$db_age,$db_zip,$db_min_offer,$db_best_chance,$db_last_action,$db_now);
	
	$array = array();
	$stmt->fetch();

	$details = $z->get_zip_details($db_zip);
	if ($details === false)
	   $area = '';
	else
	   $area = $details['city'].', '.$details['state_prefix'];
	
	if(($db_now - $db_last_action) < 60*15)
		$onlineNow = 1;
	else
		$onlineNow = 0;
		
	$array[$db_id]['name'] = $db_name;
	$array[$db_id]['age'] = $db_age;
	$array[$db_id]['zip'] = $db_zip;
	$array[$db_id]['area'] = $area;
	$array[$db_id]['onlineNow'] = $onlineNow;
	$array[$db_id]['min_offer'] = $db_min_offer;
	$array[$db_id]['best_chance'] = $db_best_chance;
	
	$stmt->close();
	
	$return[0]=1;
	$return[1]=$array;
	
	die(json_encode($return));
}

function searchProfile($currentUser) {

	global $mysqli;
	
	$frmDistance = intval($_POST['distance']);
	$frmZip = intval($_POST['zip']);
	$frmIAM = intval($_POST['iam']);
	$frmSeek = intval($_POST['seeking']);
	$frmMinAge = intval($_POST['minAge']);
	$frmMaxAge = intval($_POST['maxAge']);
	$start = intval($_GET['start']);
	$limit = intval($_GET['limit']);
	
	if(!$limit)
	    $limit=30;
		
	$return = array();
	$return[0]=0;
	
	$z = new zipcode_class;
	$whereClause = '';
	
	$zips = $z->get_zips_in_range($frmZip, $frmDistance, _ZIPS_SORT_BY_DISTANCE_ASC, true); 
	
	if(!$zips) {
		$return[1]="Invalid Request";
		die(json_encode($return));
	}
	
	$i=0;
	foreach($zips as $key => $k) {
		if($i<sizeof($zips)-1)
			$whereClause .= "zip=".$key." || ";
		else
			$whereClause .= "zip=".$key;
		$i++;
	}
	
	if($currentUser->getGroup() == '1')
		$stmt = $mysqli->prepare("SELECT t.id, t.name, t.age, t.zip, t.min_offer, t.best_chance, m.lastaction, UNIX_TIMESTAMP(NOW()) FROM profile t JOIN ( SELECT `id`,`group`,`lastaction` FROM members ) m ON t.id = m.id WHERE m.group=2 && iAm=? && seeking=? && (".$whereClause.") && `age` BETWEEN ? and ? LIMIT ?,?");
	else
		$stmt = $mysqli->prepare("SELECT t.id, t.name, t.age, t.zip, t.min_offer, t.best_chance, m.lastaction, UNIX_TIMESTAMP(NOW()) FROM profile t JOIN ( SELECT `id`,`group`,`lastaction` FROM members ) m ON t.id = m.id WHERE m.group=1 && iAm=? && seeking=? && (".$whereClause.") && `age` BETWEEN ? and ? LIMIT ?,?");

	$stmt->bind_param('iiiiii',$frmSeek, $frmIAM, $frmMinAge, $frmMaxAge, $start, $limit); //Inverted
	$stmt->execute();
	$stmt->bind_result($db_id,$db_name,$db_age,$db_zip,$db_min_offer,$db_best_chance,$db_last_action,$db_now);

	$array = array();
	
	
	while($stmt->fetch()) {
		$details = $z->get_zip_details($db_zip);
		if ($details === false)
		   $area = '';
		else
		   $area = $details['city'].', '.$details['state_prefix'];
		
		if(($db_now - $db_last_action) < 60*15)
		    $onlineNow = 1;
		else
			$onlineNow = 0;
			
		$array[$db_id]['name'] = $db_name;
		$array[$db_id]['age'] = $db_age;
		$array[$db_id]['zip'] = $db_zip;
		$array[$db_id]['area'] = $area;
		$array[$db_id]['onlineNow'] = $onlineNow;
		$array[$db_id]['min_offer'] = $db_min_offer;
		$array[$db_id]['best_chance'] = $db_best_chance;
	}
	
	$stmt->close();
	
	$return[0]=1;
	$return[1]=$array;
	
	die(json_encode($return));
	 
}

function getFeatured() {

	global $mysqli;
	$result = $mysqli->query("SELECT `id`, `name`, `age` FROM `profile` WHERE `featured` = 1");
	if(!$result->num_rows) {
		$return[0] = 0;
		$return[1] = "No featured";
		die(json_encode($return));
	}
	
	$return[0]=1;
	$return[1] = $result->fetch_assoc();
	die(json_encode($return));
	 
}

function getViewedMe($currentUser) {
	global $mysqli;
	
	$queue = new Queue(10);
	
	$stmt = $mysqli->prepare("SELECT `viewedMe` FROM `profile` WHERE id=?");
	$stmt->bind_param('i',$currentUser->getId());
	$stmt->execute();
	$stmt->bind_result($db_viewedMe);
	$stmt->fetch();
	$stmt->close();
	
	//Set the viewed me
	if($db_viewedMe) {
		$queue->populateFromJSON($db_viewedMe);
	}
	
	$viewedArr = array();
	
	while(!$queue->is_empty()) {
		$viewedArr[] = $queue->dequeue();
	}
	
	$return = array();
	$return[0]=1;
	$return[1] = $viewedArr;
	
	die(json_encode($return));
	
}


?>
