<?
class Template {
	
	private $vars;
	
	private $terminate;
	private $contentType;
	private $content;
	private $base_page;
	private $redirect;
	
	function __construct() {
		$this->vars = array(); //Associative array to hold variables.
		$this->location = _SITEROOT_."templates/";
	}
	
	function moduleTemplate($module) {
		$instance = new self();
		$instance->vars = array(); //Associative array to hold variables.
		$instance->location = _SITEROOT_."templates/module_templates/".$module."/";
		return $instance;	
	}
	
	function redirect($location) {
		$this->redirect = $location;
	}
	
	//Sets weather the script will terminate after rendering the template
	function terminate($term) {
		$this->terminate = $term;
	}
	
	//Sets and gets template reference vars
	function setVars($varName, $varData) {
		$this->vars[$varName] = $varData;
	}
	
	function getVars($varName) {
		return $this->vars[$varName];
	}
	
	//The base page denotes the "main" page of the template.  A base page provides the framework and includes things such as header and footer
	//Base page exists in ./templates
	function base_page($bp) {
		if(!file_exists($this->location.$bp.".php")) {
			die("Can not import main template");
		}
		$this->base_page = $bp;
	}
	
	//Optionally sets a contentType for content subpage inclusion.  Can be used to include a form.
	// ./templates/$contentType/
	function contentType($ct) {
		$this->contentType = $ct;
	}
	
	//Optionally sets the content for subpage inclusion.  content type must be specified first.
	// ./templates/$contentType/$content.php
	function content($content) {
		if(!$this->contentType) {
			die("Content Type not Set");
		}
		
		if(!file_exists($this->location.$this->contentType."/".$content.".php")) {
			die("Can not import content template");
		}
		
		$this->content = $content;
	}
	
	//Used to include content if $contentType and $content are set.
	function includeContent() {
		if (!isset($this->contentType) || !isset($this->content)) {
			die("Error -  ContentType or Content not specified");
		}
		
		//Check for additional functions
		if(file_exists($this->location.$this->contentType."/".$this->content."_functions.php"))
			require_once($this->location.$this->contentType."/".$this->content."_functions.php");
			
		require_once($this->location.$this->contentType."/".$this->content.".php");
	}
	
	function render() {
		if (isset($this->redirect)) {
			header("Location: "._DOMAIN_.$this->redirect.".php");
		}
		
		if (isset($this->base_page)) {
			require_once($this->location.$this->base_page.".php");
		}
		
		if($this->terminate)
			die();
	}
	
	function returnRender() {
		ob_start();
		if (isset($this->redirect)) {
			header("Location: ".$domain.$this->redirect.".php");
		}
		
		if (isset($this->base_page)) {
			require_once($this->location.$this->base_page.".php");
		}
		
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
}
?>