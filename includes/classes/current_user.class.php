<?
class CurrentUser {
	
	
	function __construct(){
		global $mysqli;
		
		$this->mysqli = $mysqli;

		if ($_SESSION['id'] && $_SESSION['group'] && $_SESSION['username'] && $_SESSION['iam']) {
			$this->id = $_SESSION['id'];
			$this->username = $_SESSION['username'];
			$this->group = $_SESSION['group'];
			$this->iam = $_SESSION['iam'];
			$this->updateLastAction();
		} else{
			$this->id = 0;
			$this->gid = 0; //Initialize to not logged in
		}
	}
	
	// Real consturctors
	public static function withID($id) {
		$instance = new self();
		$instance->id = $id;
		$instance->gid = 0; //Initialize to not logged in	
		return $instance;
	}
	
	//Setters
	function setId($id) {
		$_SESSION['id'] = $id;
		$this->id = $id;
	}
	
	function setIam() {
		$_SESSION['iam'] = $iam;
		$this->iam = $iam;
	}

	function setUsername($username) {
		$_SESSION['username'] = $username;
		$this->username = $username;
	}

	function setGroup($gid) {
		$this->group = $gid;
		$_SESSION['group'] = $gid;
	}
	
	function resetToken() {
		$_SESSION['token'] = mt_rand(0,getrandmax());
	}
	//Getters
	function getId() {
		return $this->id;
	}
	
	function getGroup() {
		return $this->group;
	}

	function getIam() {
		return $this->iam;
	}
	
	function getUsername() {
		return $this->username;
	}
		
	function getToken() {
		return $_SESSION['token'];
	}
	
	function isLoggedIn() {
		if(!$this->getGroup())
			return false;
		else
			return true;
	}
	
	function CSRFValidate($userToken) {
		if($userToken == $this->getToken())
			return true;
		else
			return false;
	}
	
	//Functions
	function updateLastAction() {
		 $stmt = $this->mysqli->prepare("UPDATE `members` SET `lastaction` = UNIX_TIMESTAMP(now()) WHERE `id` = ? LIMIT 1");
		 $stmt->bind_param('s',$this->getId());
		 $stmt->execute();
		 $stmt->close();
	}
	
	//toId is the toId to check permissions against
	/*type
		0 - Messaging Permissions
		1 - Video Permissins
	*/
	function checkPermissions($toId, $type) {
		 $stmt = $this->mysqli->prepare("SELECT `permissions` FROM `members` WHERE `id` = ? LIMIT 1");
		 $stmt->bind_param('s',$this->getId());
		 $stmt->execute();
		 $stmt->bind_result($db_permissions);
		 $stmt->fetch();
		 $stmt->close();

		 $db_permissions = json_decode($db_permissions, true);
		 
		 if($db_permissions[$toId][$type])
			return true;
		 else
			return false;
	}
	
	function retJSONInfo() {
		$return = array();
		
		if(!$this->isLoggedIn()) {
			$return['id'] = 0;
			$return['group'] = 0;
		}else{
			$return['id'] = $this->getId();
			$return['group'] = $this->getGroup();
			$return['username'] = $this->getUsername();
			$return['iam'] = $this->getIam();
			$return['token'] = $this->getToken();
		}
		
		return json_encode($return);
	}

}
?>