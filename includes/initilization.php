<?

	session_start();
	
	chdir("/home/worthyou/public_html/");
	
	define("RUNNING",1);
	
	//Includes
	include("./includes/config.php");
	include("./includes/classes/template.class.php");
	include("./includes/classes/current_user.class.php");
	include("./includes/classes/module_loader.class.php");
	
	
	//instantiate MySQLi
	$mysqli = new mysqli("localhost",$dbuser,$dbpass,$dbname);
	
	if(defined("_LEGACY_")) {
		mysql_connect('localhost',$dbuser,$dbpass) or die(mysql_error()); 
		mysql_select_db($dbname) or die(mysql_error()); 
	}
	
	//instantiate template
	$template = new Template();
	
	//Instantiate currentUser
	$currentUser = new CurrentUser();

	//Instantiate the module loader
	$moduleLoader = new ModuleLoader();
	
	chdir(_CWD_);
?>
