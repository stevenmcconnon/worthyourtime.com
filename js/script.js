$(document).ready(function() {
   $('ul#menu').superfish({
      delay:       400,
      animation:   {opacity:'show',height:'show'},
      speed:       400,
      autoArrows:  false,
      dropShadows: false
   });
 });