function processLogin(userName, password, domain, callback) {
	
	loginUrl = domain + "backend/login.php";
   	
	$.post(loginUrl, { username: userName, password: password  }, function(responseText) {
			console.log("response is " + responseText);
			if (responseText[0] == '0') {
				alert(responseText[1]);
				console.log("ERROR LOGGING IN: username=" + userName + " pw=" + password + " loginUrl=" + loginUrl);
			} 
			if(callback)
				callback(responseText);
		
	}, "json");
	
}
